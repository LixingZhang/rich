<?php

namespace App\Models;

use App\Scopes\TenantScope;
use App\Traits\BelongsToTenant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Order extends Model
{
    use SoftDeletes, HasFactory, BelongsToTenant;

    const STATUS_ORDERED = 'ordered';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_MANUFACTURING = 'manufacturing';
    const STATUS_COMPLETED = 'completed';

    const STATUSES = [
        self::STATUS_ORDERED => 'Ordered',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_MANUFACTURING => 'Manufacturing',
        self::STATUS_COMPLETED => 'Completed'
    ];

    protected $casts = ['eta' => 'date'];

    protected $appends = ['eta_for_editing'];

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->status = self::STATUS_ORDERED;
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function shippingAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function billingAddress()
    {
        return $this->belongsTo(Address::class);
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }

    public function complete()
    {
        $this->update(['status' => Order::STATUS_COMPLETED]);
    }

    public function confirm()
    {
        $this->update(['status' => Order::STATUS_CONFIRMED]);
    }

    public function getStatusColourAttribute(): string
    {
        return [
            self::STATUS_ORDERED => 'indigo'
        ][$this->status] ?? 'cool-gray';
    }

    public function getDateForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * @param Quote $quote
     *
     * @throws \Exception
     */
    public static function createFromQuote(Quote $quote)
    {
        foreach ([
            BasswoodShutter::class,
            PvcShutter::class,
            AuPvcShutter::class,
            AluminiumShutter::class,
            RollerBlind::class
        ] as $productClass) {
            try {
                $products = $quote->getProductCollection($productClass)->all();
                if (!empty($products)) {
                    DB::beginTransaction();
                    if ($customerQuote = $quote->customerQuote) {
                        $totalItems = $quote->items->count();
                        if ($customerQuote->markup_type == CustomerQuote::MARKUP_FIXED) {
                            $fixedMarkup = $customerQuote->markup_value / $totalItems;
                        } else {
                            $percentMarkup = $customerQuote->markup_value;
                        }
                    }

                    $order = Order::make([
                        'reference'  => $quote->reference,
                        'notes'         => $quote->notes,
                        'product'       => $productClass::NAME
                    ]);

                    $order->tenant()->associate($quote->tenant);

                    if ($billingAddress = $quote->tenant->billingAddress) {
                        $order->billingAddress()->associate($billingAddress);
                    }
                    if ($shippingAddress = $quote->tenant->shippingAddress) {
                        $order->shippingAddress()->associate($shippingAddress);
                    }

                    $items = [];
                    foreach ($products as $product) {
                        $item = OrderItem::createFromProduct($product);
                        if (isset($fixedMarkup)) {
                            $item->markup_value = $fixedMarkup;
                        } elseif (isset($percentMarkup)) {
                            $item->markup_value = $item->amount * $percentMarkup / 100;
                        }
                        $item->amount = $item->unit_price - $item->discount_value + $item->markup_value;
                        $order->amount += $item->amount;
                        $items[] = $item;
                    }
                    $order->staff()->associate(auth()->user());
                    $order->save();
                    $order->items()->saveMany($items);
                    ConvertedQuote::create([
                        'quote_id' => $quote->id,
                        'order_id' => $order->id
                    ]);
                    DB::commit();
                }
            } catch (\Exception $e) {
                DB::rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param Tenant $tenant
     * @param array  $productData
     * @param string $productClass
     * @param string $reference
     * @param string $notes
     *
     * @throws \Exception
     */
    public static function createFromFormData(
        Tenant $tenant,
        array $productData,
        string $productClass,
        string $reference,
        string $notes
    ) {
        try {
            if (!empty($productData)) {
                DB::beginTransaction();
                $order = Order::make([
                    'reference'  => $reference,
                    'notes'         => $notes,
                    'product'       => $productClass::NAME
                ]);

                $order->tenant()->associate($tenant);
                $order->staff()->associate(auth()->user());
                if ($tenant->billingAddress) {
                    $order->billingAddress()->associate($tenant->billingAddress);
                }
                if ($tenant->shippingAddress) {
                    $order->shippingAddress()->associate($tenant->shippingAddress);
                }

                $items = [];
                foreach ($productData as $shutter) {
                    $item = OrderItem::createFromProduct($productClass::create($shutter));
                    $item->amount = $item->unit_price - $item->discount_value + $item->markup_value;
                    $order->amount += $item->amount;
                    $order->save();
                    $items[] = $item;
                }
                $order->items()->saveMany($items);
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getStatuses(): array
    {
        switch ($this->status) {
            case static::STATUS_CONFIRMED:
                return [
                    static::STATUS_CONFIRMED,
                    static::STATUS_MANUFACTURING,
                    static::STATUS_COMPLETED
                ];
            case static::STATUS_MANUFACTURING:
                return [
                    static::STATUS_MANUFACTURING,
                    static::STATUS_COMPLETED
                ];
            default:
                return [
                    static::STATUS_ORDERED,
                    static::STATUS_CONFIRMED,
                    static::STATUS_MANUFACTURING,
                    static::STATUS_COMPLETED,
                ];
        }
    }

    public function getEtaForEditingAttribute()
    {
        return optional($this->eta)->format('d/m/Y');
    }

    public function setEtaForEditingAttribute($value)
    {
        if ($value) {
            $this->eta = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
}
