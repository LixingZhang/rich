<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable, HasFactory;

    const ROLE_USER = 'User';
    const ROLE_ADMIN = 'Administrator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static array $roles = [
        self::ROLE_ADMIN,
        self::ROLE_USER
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->password = Hash::make('password');
        $this->role = self::ROLE_USER;
    }

    public function tenants()
    {
        return $this->belongsToMany(Tenant::class, 'tenant_users');
    }

    public function hasRole($role): bool
    {
        return $this->role == $role;
    }

    public function isAdmin(): bool
    {
        return $this->role == static::ROLE_ADMIN;
    }

    public function isUser(): bool
    {
        return $this->role == static::ROLE_USER;
    }

    public function isTenant(): bool
    {
        return count($this->tenants) >= 1;
    }

    public function avatarUrl(): string
    {
        return 'https://www.gravatar.com/avatar/'.md5(strtolower(trim($this->email)));
    }

    /**
     * @return Tenant|null
     */
    public function primaryTenant(): ?Tenant
    {
        if (!$this->isTenant()) {
            return null;
        }

        return Tenant::query()
            ->join('tenant_users', 'tenants.id', '=', 'tenant_users.tenant_id')
            ->where('tenants.is_primary', true)
            ->where('tenant_users.user_id', $this->id)
            ->first();
    }

    public function getCustomersAttribute()
    {
        if (!$this->isTenant()) {
            return Customer::all();
        }

        return Customer::whereIn('tenant_id', $this->tenants->pluck('id'));
    }
}
