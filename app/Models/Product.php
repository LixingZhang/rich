<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class Product extends Model
{
    use HasFactory;

    const NAME = 'Product';

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'productable');
    }

    public function quoteItem()
    {
        return $this->morphOne(QuoteItem::class, 'productable');
    }

    public static function rules()
    {
        return [
            'id' => 'integer',
            'name' => 'required|max:255',
            'width' => 'required|integer',
            'drop' => 'required|integer',
        ];
    }

    abstract public function calculatePrice();

    public function getName(): string
    {
        return static::NAME;
    }
}
