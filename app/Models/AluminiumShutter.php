<?php

namespace App\Models;

use Illuminate\Validation\Rule;

class AluminiumShutter extends Shutter
{
    const NAME = 'Aluminium Shutter';

    public static array $tiltRods = [
        'Clear View',
    ];

    public static array $frames = [
        'No Frame',
        'L',
        'U Channel',
        'Bi Fold',
        'Sliding',
    ];

    public static array $colours = [
        'White',
        'Cream',
        'Silver',
    ];

    /**
     *
     * @param $drop
     *
     * @return string[]
     */
    public static function midRails($drop)
    {
        if ($drop <= 2299 && $drop >= 1800) {
            return [
                'NA',
                'Centre',
                '1',
            ];
        } elseif ($drop <= 2599 && $drop >= 2300) {
            return [
                '2',
            ];
        } elseif ($drop <= 2600 && $drop >= 3000) {
            return [
                '3',
            ];
        } else {
            return [
                'NA',
                'Centre',
                '1',
            ];
        }
    }

    public static array $bladeSizes = [
       '89mm'
    ];

    public static function rules()
    {
        return parent::rules() + [
                'width' => 'required|integer|min:250',
                'drop' => 'required|integer|max:3050|min:250',
                'mid_rail' => [
                    'required',
                    Rule::in('NA', 'Centre', '1', '2')
                ],
                'mid_rail_height' => [

                ],
                'blade_size' => [
                    'required',
                     Rule::in('89mm')
                ],
                'tilt_rod' => [
                    'required',
                    Rule::in('Clear View')
                ],
                'colour' => [
                    'required',
                    Rule::in('White', 'Cream', 'Silver')
                ],
                'frame' => [
                    'required',
                    Rule::in('No Frame','L', 'U Channel', 'Bi Fold')
                ],

            ];
    }
}
