<?php

namespace App\Models;

use App\Service\RollerBlindPriceCalculator;
use Illuminate\Validation\Rule;

class RollerBlind extends Product
{
    const NAME = 'Roller Blind';

    protected $guarded = [];

    public static array $types = [
        'Blockout',
        'Screen',
        'Light Filter',
    ];

    public static array $fabrics = [
        'Blockout'     => [
            'Solitaire',
            'Focus',
            'Metroshade',
            'Sanctuary',
            'Riviera',
            'Tusk',
            'Jersey',
            'Balmoral',
        ],
        'Screen'       => [
            'One Screen',
            'Solar View',
        ],
        'Light Filter' => [
            'Metroshade',
            'Riviera',
            'Tusk',
            'Jersey',
            'Balmoral',
        ]
    ];

    public static array $colours = [
        'Blockout' => [
            'Solitaire'  => [
                'Snow',
                'Antique White',
                'Champagne',
                'Granite',
                'Peat',
                'Pepper',
                'Coffee',
                'Concord',
                'Ironstone',
                'Onyx',
            ],
            'Focus'      => [
                'White',
                'Chalk',
                'Shell',
                'Dove',
                'Linen',
                'Cream',
                'Powder',
                'Putty',
                'Stone',
                'Cotton',
                'Alabaster',
                'Almond',
                'Sandstone',
                'Latte',
                'Beechwood',
                'Cloud',
                'Pumice',
                'Oyster',
                'Eucalypt',
                'Duck Egg',
                'Smoke',
                'Ash',
                'Blush',
                'Jarrah',
                'Espresso',
                'Fig',
                'Tempest',
                'Coal',
                'Ebony',
            ],
            'Metroshade' => [
                'Dove/White',
                'Ecru',
                'Nougat',
                'Dune',
                'Pebble',
                'Seal',
                'Quill',
                'Ice Grey',
                'Moonstone',
                'Slate',
                'Storm',
                'Black',
            ],
            'Sanctuary'  => [
                'Plaster',
                'Marble',
                'Limestone',
                'Ceramic',
                'Truffle',
                'Fossil',
                'Suede',
                'Mineral',
                'Slate',
                'Baltic',
                'Lava',
            ],
            'Riviera'    => [
                'Cannes',
                'Nice',
                'La Palme',
                'St Marie',
                'Monte Carlo',
                'Cassis',
            ],
            'Tusk'       => [
                'Mushroom',
                'Pepper',
                'Beach',
                'Coconut',
                'Slate',
                'Whisper',
                'Hazelnut',
                'Henna',
            ],
            'Jersey'     => [
                'Opal',
                'Organic',
                'Stone',
                'Render',
                'Timber',
                'Pavement',
                'Steel',
            ],
            'Balmoral'   => [
                'White',
                'Pearl',
                'Birch',
                'Bourneville',
                'Concrete',
                'Dove',
                'Jet',
                'Putty',
                'Pyrite',
                'Steel',
            ]
        ],
        'Screen'   => [
            'One Screen'   => [
                'White',
                'Sand',
                'Dune',
                'Linen/Bronze',
                'Wallaby',
                'Ice',
                'Grey',
                'Gunmetal',
                'Silver/Black',
                'Mercury',
                'Charcoal',
                'Black',
            ],
            'Solar View'   => [
                'White',
                'Off White',
                'Alabaster',
                'Cotton',
                'Bone/Off White',
                'Taupe',
                'Platinum',
                'Bronze/Charcoal',
                'Black/Copper',
                'Grey',
                'Charcoal',
                'Black',
            ],
            'Light Filter' => [
                'Metroshade' => [
                    'Dove/White',
                    'Ecru',
                    'Nougat',
                    'Ice Grey',
                    'Quill',
                    'Moonstone',
                ],
                'Riviera'    => [
                    'Cannes',
                    'Nice',
                    'La Palme',
                    'St Marie',
                    'Monte Carlo',
                    'Cassis',
                ],
                'Tusk'       => [
                    'Mushroom',
                    'Pepper',
                    'Beach',
                    'Coconut',
                    'Slate',
                    'Whisper',
                    'Hazelnut',
                    'Henna',
                ],
                'Jersey'     => [
                    'Opal',
                    'Organic',
                    'Stone',
                    'Render',
                    'Timber',
                    'Pavement',
                    'Steel',
                ],
                'Balmoral'   => [
                    'White',
                    'Pearl',
                    'Birch',
                    'Bourneville',
                    'Concrete',
                    'Dove',
                    'Jet',
                    'Putty',
                    'Pyrite',
                    'Steel',
                ]
            ]
        ]
    ];

    public static array $controlTypes = [
        'Chain',
        'Motor'
    ];

    public static array $controlSides = [
        'Left',
        'Right'
    ];

    public static array $componentColours = [
        'White',
        'Black'
    ];

    public static array $baseRails = [
        'Silver',
        'Black'
    ];

    public static array $rollDirections = [
        'Standard',
        'Reverse'
    ];

    public static array $motorTypes = [
        'NA',
        'Acmeda 240v',
        'Acmeda Li Ion 1.1nm',
        'Acmeda Li Ion 3.0nm'
    ];

    public static array $chargers = [
        'NA',
        'Yes'
    ];

    public static array $wifiHubs = [
        'NA',
        'Acmeda Pulse'
    ];

    public static array $remotes = [
        'NA',
        '1 Channel',
        '15 Channel'
    ];

    public static array $bracketOptions = [
        'Single',
        'Slim Combo Top Front',
        'Slim Combo Top Back',
        'Double Bracket'
    ];

    public static function rules()
    {
        return [
            'name'             => 'required|max:10',
            'width'            => 'required|integer|max:3010',
            'drop'             => 'required|integer|max:3300',
            'type'             => [
                'required',
                Rule::in(['Blockout', 'Screen', 'Light Filter'])
            ],
            'fabric'           => [
                'required'
            ],
            'colour'           => [
                'required'
            ],
            'control_type'     => [
                'required',
                Rule::in('Chain', 'Motor')
            ],
            'control_side'     => [
                'required',
                Rule::in('Left', 'Right')
            ],
            'chain_length'     => [
                'required',
            ],
            'component_colour' => [
                'required',
                Rule::in(
                    'White',
                    'Black'
                )
            ],
            'base_rail'        => [
                'required',
                Rule::in(
                    'Silver',
                    'Black'
                )
            ],
            'roll_direction'   => [
                'required',
                Rule::in(
                    'Standard',
                    'Reverse'
                )
            ],
            'motor_type'       => [
                'required',
                Rule::in(
                    'NA',
                    'Acmeda 240v',
                    'Acmeda Li Ion 1.1nm',
                    'Acmeda Li Ion 3.0nm'
                )
            ],
            'charger'          => [
                'required',
                Rule::in(
                    'NA',
                    'Yes'
                )
            ],
            'wifi_hub'         => [
                'required',
                Rule::in(
                    'NA',
                    'Acmeda Pulse'
                )
            ],
            'remote'           => [
                'required',
                Rule::in(
                    'NA',
                    '1 Channel',
                    '15 Channel'
                )
            ],
            'bracket_options'   => [
                'required',
                Rule::in(
                    'Single',
                    'Slim Combo Top Front',
                    'Slim Combo Top Back',
                    'Double Bracket'
                )
            ],
            'notes'            => [

            ]
        ];
    }

    public function calculatePrice()
    {
        $shutterPrice = new RollerBlindPriceCalculator($this);

        return $shutterPrice->getCost();
    }
}
