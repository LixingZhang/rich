<?php

namespace App\Models;

use Illuminate\Validation\Rule;

class AuPvcShutter extends Shutter
{
    const NAME = 'AU PVC Shutter';

    public static array $frames = [
        'No Frame',
        'Z20-C6',
        'BL50-B10',
        'BL65-B10A',
    ];

    public static array $stileTypes = [
        '50b',
    ];

    public static array $colours = [
        'White',
        'Off White',
        'Custom',
    ];

    public static array $tiltRods = [
        'Clear View',
    ];

    /**
     *
     * @param $drop
     *
     * @return string[]
     */
    public static function midRails($drop)
    {
        if (!$drop) {
            return [];
        }

        if ($drop <= 1999 && $drop >= 1600) {
            return [
                'NA',
                'Centre',
                '1',
            ];
        } elseif ($drop <= 2440 && $drop >= 2000) {
            return [
                '2',
            ];
        }

        return [];
    }

    public static array $bladeSizes = [
        '89mm'
    ];

    public static array $hingeTypes = [
        'NA',
        'Non Mortised',
        'Pivot',
    ];

    public static array $hingeColours = [
        'White',
    ];

    public static function rules()
    {
        return [
            'name'            => 'required|max:10',
            'width'           => 'required|integer|min:250',
            'drop'            => 'required|integer|max:2440|min:250',
            'mid_rail'        => [
                'required',
                Rule::in('NA', 'Centre', '1', '2')
            ],
            'mid_rail_height' => [

            ],
            'blade_size'      => [
                'required',
                Rule::in('89mm')
            ],
            'tilt_rod'        => [
                'required',
                Rule::in('Clear View')
            ],
            'colour'          => [
                'required',
                Rule::in(
                    'White',
                    'Off White',
                    'Custom'
                )
            ],
            'stile_type'      => [
                'required',
                Rule::in('50b')
            ],
            'frame'           => [
                'required',
                Rule::in('No Frame', 'Z20-C6', 'BL50-B10', 'BL65-B10A')
            ],
            'frame_options'   => [
                'required',
                Rule::in('NA', 'LRTB', 'LRT', 'LRB')
            ],
            'hinge_type'      => [
                'required',
                Rule::in('NA', 'Non Mortised', 'Pivot')
            ],
            'hinge_colour'    => [
                'required',
                Rule::in('White')
            ]
        ];
    }
}
