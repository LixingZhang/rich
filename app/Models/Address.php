<?php

namespace App\Models;

use App\Traits\BelongsToTenant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory, BelongsToTenant;

    protected $guarded = [];

    public static array $states = [
        'NSW',
        'NT',
        'QLD',
        'SA',
        'TAS',
        'VIC',
        'WA',
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
