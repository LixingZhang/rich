<?php

namespace App\Models;

use App\Service\ShutterPriceCalculator;
use Illuminate\Validation\Rule;

abstract class Shutter extends Product
{
    protected $table = 'shutters';

    protected $guarded = [];

    public static array $shutterTypes = [
        'Standard',
        'Sliding',
        'Bifold Track',
        'U Channel',
    ];

    public static array $corners = [
        'No',
        '90',
        '135',
    ];

    public static array $inOrOut = [
        'In',
        'Out'
    ];

    public static array $frameOptions = [
        'NA',
        'LRTB',
        'LRT',
        'LRB',
    ];

    public static function rules()
    {
        return [
            'name' => 'required|max:255',
            'shutter_type' => [
                'required',
                Rule::in(
                    'Standard',
                    'Sliding',
                    'Bifold Track',
                    'U Channel'
                )
            ],
            'corner' => [
                'required',
                Rule::in('No', '90', '135')
            ],
            'panel_layout' => [
                'required',
                'regex:/^[C|D|L|R|T|-]+$/i'
            ],
            'in_or_out' => [
                'required',
                Rule::in('In', 'Out')
            ],
        ];
    }

    public function calculatePrice()
    {
        $shutterPrice = new ShutterPriceCalculator($this);
        return $shutterPrice->getCost();
    }

    public function setPanelLayoutAttribute($value)
    {
        $this->attributes['panel_layout'] = strtoupper($value);
        preg_match_all('/[L|R]+/', $this->attributes['panel_layout'], $matches);
        $count = array_reduce(current($matches), function ($carry, $match) {
            $carry += strlen($match);

            return $carry;
        }, 0);

        $this->attributes['panel_qty'] = $count;
    }

}
