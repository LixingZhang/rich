<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    const STATUS_PAID = 'Paid';
    const STATUS_INVOICED = 'Invoiced';

    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
