<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteItem extends Item
{
    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }
}
