<?php

namespace App\Models;

use App\Traits\BelongsToTenant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory, BelongsToTenant;

    protected $guarded = [];

    public function quotes()
    {
        return $this->hasMany(CustomerQuote::class);
    }

    public function getLocationAttribute()
    {
        return "{$this->city} {$this->state} {$this->postcode}";
    }
}
