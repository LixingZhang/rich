<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class Item extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['productable'];

    public function discount()
    {
        return $this->belongsTo(ProductDiscount::class, 'discount_id');
    }

    public function productable()
    {
        return $this->morphTo();
    }

    public static function createFromProduct(Product $product)
    {
        $item = new static;
        $item->productable()->associate($product);
        $item->unit_price = $product->calculatePrice();

        $discount = ProductDiscount::where('product', $product->getName())->first();

        if ($discount) {
            $item->discount()->associate($discount);
            $item->discount_value = $item->unit_price * $discount->discount / 100;
        }

        $item->amount = $item->unit_price - $item->discount_value;

        return $item;
    }
}
