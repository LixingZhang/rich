<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Tenant extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class, 'tenant_users');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function shippingAddress()
    {
        return $this->belongsTo(Address::class, 'shipping_address_id');
    }

    public function bankAccount()
    {
        return $this->hasOne(BankAccount::class);
    }

    public function logoUrl(): string
    {
        return $this->logo
            ? Storage::disk('logos')->url($this->logo)
            : 'https://ui-avatars.com/api/?name='. urlencode($this->name);
    }

    public function setPrimary()
    {
        $this->update(['is_primary' => true]);
    }

    public function unsetPrimary()
    {
        $this->update(['is_primary' => false]);
    }

    public function isPrimary(): bool
    {
        return $this->is_primary;
    }
}
