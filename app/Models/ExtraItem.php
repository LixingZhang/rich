<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExtraItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function quote()
    {
        return $this->belongsTo(CustomerQuote::class);
    }
}
