<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvertedQuote extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }
}
