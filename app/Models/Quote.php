<?php

namespace App\Models;

use App\Traits\BelongsToTenant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
    use SoftDeletes, HasFactory, BelongsToTenant;

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:Y-m-d',
        'updated_at'  => 'date:Y-m-d',
        'deleted_at'   => 'date:Y-m-d',
    ];

    public function items()
    {
        return $this->hasMany(QuoteItem::class);
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function customerQuote()
    {
        return $this->hasOne(CustomerQuote::class);
    }

    public function convertedQuotes()
    {
        return $this->hasMany(ConvertedQuote::class);
    }

    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }

    public function getBasswoodShuttersAttribute()
    {
        $this->getProductCollection(BasswoodShutter::class);
    }

    public function getPvcShuttersAttribute()
    {
        $this->getProductCollection(PvcShutter::class);
    }

    public function getAuPvcShuttersAttribute()
    {
        $this->getProductCollection(AuPvcShutter::class);
    }

    public function getAluminiumShuttersAttribute()
    {
        $this->getProductCollection(AluminiumShutter::class);
    }

    public function getRollerBlindsAttribute()
    {
        $this->getProductCollection(RollerBlind::class);
    }

    public function getProductCollection(string $productClass)
    {
        return $this->items()
            ->where('productable_type', $productClass)
            ->with('productable')
            ->get()
            ->pluck('productable');
    }

    public function getDateForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
