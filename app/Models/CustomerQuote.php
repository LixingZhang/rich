<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerQuote extends Model
{
    use HasFactory;

    public const MARKUP_FIXED = 'fixed';
    public const MARKUP_PERCENT = 'percent';

    public const STATUS_PENDING = 'pending';
    public const STATUS_APPROVED = 'approved';

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->status = self::STATUS_PENDING;
    }

    public function quote()
    {
        return $this->belongsTo(Quote::class);
    }

    public function extraItems()
    {
        return $this->hasMany(ExtraItem::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getDateForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function approve()
    {
        $this->update([
            'status'      => CustomerQuote::STATUS_APPROVED,
            'approved_at' => new \DateTime()
        ]);
    }

    public function isApproved(): bool
    {
        return $this->status == CustomerQuote::STATUS_APPROVED;
    }

    public function getReferenceAttribute()
    {
        return $this->quote->reference;
    }

    public function getTenantAttribute()
    {
        return $this->quote->tenant;
    }

    public function getCustomerNameAttribute()
    {
        return $this->quote->customer_name;
    }

    public function getItemsAttribute()
    {
        return $this->quote->items;
    }

    public function getAmountAttribute()
    {
        return $this->quote->amount;
    }

    public function getStatusColourAttribute(): string
    {
        return [
                   self::STATUS_PENDING  => 'indigo',
                   self::STATUS_APPROVED => 'green',
               ][$this->status] ?? 'cool-gray';
    }
}
