<?php

namespace App\Service;

use App\Models\ProductDiscount;
use App\Models\RollerBlind;
use App\Models\RollerBlindPricing;
use App\Models\RollerBlindPricingGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class RollerBlindPriceCalculator
{

    /**
     * @var RollerBlind
     */
    private $rollerBlind;

    public function __construct(RollerBlind $rollerBlind)
    {
        $this->rollerBlind = $rollerBlind;
    }

    private function area()
    {
        return $this->rollerBlind->width / 1000 * $this->rollerBlind->drop / 1000;
    }

    public function getCost()
    {
        return $this->getUnitPrice() * $this->area();
    }

    public function getPricingGroup()
    {
        return Cache::remember(sprintf('%s_%s_%s',
            $this->rollerBlind->fabric, $this->rollerBlind->type,
            $this->rollerBlind->colour), now()->addHour(), function () {
            $pricing = RollerBlindPricing::where(
                'fabric', $this->rollerBlind->fabric)
                ->where('type', $this->rollerBlind->type)
                ->where('colour', $this->rollerBlind->colour)
                ->firstOrFail();

            return $pricing->price_group;
        });


//        return $pricing->price_group;
    }

    public function getUnitPrice()
    {
        return Cache::remember(sprintf('%s_%s',
            $this->rollerBlind->width,
            $this->rollerBlind->drop
        ), now()->addHour(), function () {
            $group = RollerBlindPricingGroup::where(
                'group', $this->getPricingGroup()
            )->where('width_lower_threshold', '<', $this->rollerBlind->width)
                ->where('width_upper_threshold', '>=', $this->rollerBlind->width)
                ->where('drop_lower_threshold', '<', $this->rollerBlind->drop)
                ->where('drop_upper_threshold', '>=', $this->rollerBlind->drop)
                ->firstOrFail();

            return $group->price;
        });

    }

    public function getDiscountLevel()
    {
        $user = Auth::user();

        $productDiscount = ProductDiscount::where('tenant_id', $user->tenant)
            ->where('product_type', $this->rollerBlind->getName())
            ->firstOrFail();

        return $productDiscount->discount_level;
    }

//    private function getAddon()
//    {
//        $addon = $this->config['addon'];
//
//        $motorType = $addon['motor_type'][$this->criteria['motor_type']];
//        $charger = $addon['charger'][$this->criteria['charger']];
//        $wifiHub = $addon['wifi_hub'][$this->criteria['wifi_hub']];
//        $remote = $addon['remote'][$this->criteria['remote']];
//        $bracketOptions = $addon['bracket_options'][$this->criteria['bracket_options']];
//
//        return $motorType + $charger + $wifiHub + $remote + $bracketOptions;
//    }
}
