<?php


namespace App\Service;


use App\Models\ProductDiscount;
use App\Models\Shutter;
use App\Models\ShutterPricing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ShutterPriceCalculator
{
    const MIN_AREA = 0.5;

    private Shutter $shutter;

    public function __construct(Shutter $shutter)
    {
        $this->shutter = $shutter;
    }

    private function area()
    {
        return $this->shutter->width / 1000 * $this->shutter->drop / 1000;
    }

    public function getCost()
    {

        return $this->area() * $this->getUnitPrice();
    }

    public function getUnitPrice()
    {
        return Cache::remember(sprintf('%s_%s_%s', $this->shutter->shutter_type, $this->shutter->frame,
            $this->shutter->corner), now()->addHour(), function () {
            $unitPrice = 0;
            $priceGroup = ShutterPricing::where('shutter', $this->shutter->getName())
                ->where('attribute', 'Shutter Type')
                ->where('value', $this->shutter->shutter_type)
                ->firstOrFail();

            $unitPrice += $priceGroup->price;

            $priceGroup = ShutterPricing::where('shutter', $this->shutter->getName())
                ->where('attribute', 'Frame')
                ->where('value', $this->shutter->frame)
                ->firstOrFail();

            $unitPrice += $priceGroup->price;

            $priceGroup = ShutterPricing::where('shutter', $this->shutter->getName())
                ->where('attribute', 'Corner')
                ->where('value', $this->shutter->corner)
                ->firstOrFail();

            $unitPrice += $priceGroup->price;

            return $unitPrice;
        });
    }

    public function getDiscountLevel()
    {
        $user = Auth::user();

        $productDiscount = ProductDiscount::where('tenant_id', $user->tenant)
            ->where('product_type', $this->shutter->getName())
            ->firstOrFail();

        return $productDiscount->discount_level;
    }
}
