<?php

namespace App\Scopes;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class TenantScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder  $builder
     * @param  Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            return;
        }

        if (!$user->isTenant()) {
            return;
        }

        $tenants = $user->tenants->pluck('id');

        $builder->whereIn('tenant_id', $tenants);
    }
}
