<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Http\Livewire\DataTable\WithSorting;
use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Livewire\Component;

class Orders extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public bool $showEditModal = false;
    public bool $showFilters = false;

    public Order $editing;

    public array $filters = [
        'search'     => '',
        'status'     => '',
        'tenant'     => '',
        'amount-min' => null,
        'amount-max' => null,
        'date-min'   => null,
        'date-max'   => null,
    ];

    protected $queryString = ['sorts'];

    public function mount()
    {
        $this->editing = Order::make();
    }

    public function rules()
    {
        return [
            'editing.status' => 'required',
            'editing.eta_for_editing' => 'required'
        ];
    }

    public function updatedFilters()
    {
        $this->resetPage();
    }

    public function resetFilters()
    {
        $this->reset('filters');
    }

    public function edit(Order $order)
    {
        $this->useCachedRows();

        if ($this->editing->isNot($order)) {
            $this->editing = $order;
        }

        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();

        $this->editing->save();

        $this->showEditModal = false;

        $this->notify('Order Updated!');
    }

    public function getRowsQueryProperty(): Builder
    {
        $query = Order::with('tenant')->with('staff')
            ->select('orders.*')
            ->join('tenants', 'tenants.id', '=', 'orders.tenant_id')
            ->join('users', 'users.id', '=', 'orders.staff_id')
            ->when($this->filters['status'], fn($query, $status) => $query->where('status', $status))
            ->when($this->filters['amount-min'], fn($query, $amount) => $query->where('amount', '>=', $amount))
            ->when($this->filters['amount-max'], fn($query, $amount) => $query->where('amount', '<=', $amount))
            ->when($this->filters['date-min'], fn($query, $date) => $query->where('date', '>=', Carbon::parse($date)))
            ->when($this->filters['date-max'], fn($query, $date) => $query->where('date', '<=', Carbon::parse($date)))
            ->when($this->filters['search'], fn($query, $search) => $query->where('po_reference', 'like', '%' . $search . '%')
                ->orWhere('tenants.name', 'like', '%' . $search . '%'));

        if (empty($this->sorts)) {
            $query->orderByDesc('created_at');
        }

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.orders', [
            'orders' => $this->rows
        ]);
    }
}
