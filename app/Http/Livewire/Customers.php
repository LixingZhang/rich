<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Http\Livewire\DataTable\WithSorting;
use App\Models\Customer;
use Livewire\Component;

class Customers extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;

    public Customer $customer;

    public array $filters = [
        'search' => '',
    ];

    public function rules()
    {
        return [
            'customer.tenant_id'    => [
                'required',
                'integer'
            ],
            'customer.name'         => [
                'required',
                'string'
            ],
            'customer.email'        => [
                'required',
                'email',
            ],
            'customer.contact_name' => [
                'required',
                'string'
            ],
            'customer.address'      => [
                'required',
                'string'
            ],
            'customer.city'         => [
                'required',
                'string'
            ],
            'customer.postcode'     => [
                'required',
                'string'
            ],
            'customer.state'        => [
                'required',
                'string'
            ],
        ];
    }

    public function getRowsQueryProperty()
    {
        $query = Customer::with('tenant')
            ->select('customers.*')
            ->join('tenants', 'tenants.id', '=', 'customers.tenant_id')
            ->when($this->filters['search'],
                fn($query, $search) => $query->where('tenants.name', 'like', '%' . $search . '%')
                    ->orWhere('customers.name', 'like', '%' . $search . '%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function newCustomer()
    {
        $this->customer = Customer::make(['tenant_id' => '', 'state' => '']);

        $this->showEditModal = true;
    }

    public function save()
    {
        $this->validate();

        $this->customer->save();

        $this->showEditModal = false;

        $this->notify('Customer created');
    }

    public function render()
    {
        return view('livewire.customers', [
            'customers' => $this->rows
        ]);
    }
}
