<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;
use Illuminate\Support\Carbon;
use App\Models\ProductDiscount;
use Illuminate\Validation\Rule;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithPerPagePagination;

class ProductDiscounts extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;

    public ProductDiscount $editing;

    public array $filters = [
        'search'     => '',
    ];

    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->editing = new ProductDiscount;
    }

    public function rules()
    {
        return [
            'editing.tenant_id' => [
                'required',
                Rule::unique('product_discounts', 'tenant_id')->ignore($this->editing)->where(function ($query) {
                    return $query->where('product', $this->editing->product);
                })
            ],
            'editing.product'  => [
                'required',
                Rule::unique('product_discounts', 'product')->ignore($this->editing)->where(function ($query) {
                    return $query->where('tenant_id', $this->editing->tenant_id);
                })
            ],
            'editing.discount' => 'required|numeric|gt:0|lt:100'
        ];
    }

    public function create()
    {
        Gate::authorize('create', ProductDiscount::class);

        $this->useCachedRows();

        $this->editing = ProductDiscount::make(['tenant_id' => '', 'product' => '']);

        $this->showEditModal = true;

        $this->resetValidation();
    }


    public function edit(ProductDiscount $productDiscount)
    {
        Gate::authorize('create', ProductDiscount::class);

        $this->useCachedRows();

        $this->editing = $productDiscount;

        $this->showEditModal = true;
    }

    public function save()
    {
        Gate::authorize('create', ProductDiscount::class);

        $this->validate();

        $this->editing->save();

        $this->showEditModal = false;
    }

    public function getRowsQueryProperty()
    {
        $query = ProductDiscount::with('tenant')
            ->select('product_discounts.*')
            ->join('tenants', 'tenants.id', '=', 'product_discounts.tenant_id')
            ->when($this->filters['search'], fn($query, $search) => $query->where('tenants.name', 'like', '%' . $search . '%')
                    ->orWhere('product_discounts.product', 'like', '%' . $search . '%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->delete();

        $this->showDeleteModal = false;

        $this->notify('You\'ve deleted ' . $deleteCount . ' rows');
    }

    public function render()
    {
        return view('livewire.product-discounts', [
            'productDiscounts' => $this->rows
        ]);
    }
}
