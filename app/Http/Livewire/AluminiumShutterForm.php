<?php

namespace App\Http\Livewire;

use App\Models\AluminiumShutter;
use Illuminate\Validation\Rule;
use Livewire\Component;

class AluminiumShutterForm extends Component
{
    public AluminiumShutter $editing;

    protected $listeners = [
        'addProduct' => 'addAluminiumShutter',
        'setProductData' => 'setProductData'
    ];

    public function rules()
    {
        return [
            'editing.name' => 'required|max:255',
            'editing.width' => 'required|integer|min:250',
            'editing.drop' => 'required|integer|max:3050|min:250',
            'editing.shutter_type' => [
                'required',
                Rule::in(
                    'Standard',
                    'Sliding',
                    'Bifold Track',
                    'U Channel'
                )
            ],
            'editing.corner' => [
                'required',
                Rule::in('No', '90', '135')
            ],
            'editing.panel_layout' => [
                'required',
                'regex:/^[C|D|L|R|T|-]+$/i'
            ],
            'editing.in_or_out' => [
                'required',
                Rule::in('In', 'Out')
            ],
            'editing.mid_rail' => [
                'required',
                Rule::in('NA', 'Centre', '1', '2')
            ],
            'editing.mid_rail_height' => [

            ],
            'editing.blade_size' => [
                'required',
                Rule::in('89mm')
            ],
            'editing.tilt_rod' => [
                'required',
                Rule::in('Clear View')
            ],
            'editing.colour' => [
                'required',
                Rule::in('White', 'Cream', 'Silver')
            ],
            'editing.frame' => [
                'required',
                Rule::in('No Frame','L', 'U Channel', 'Bi Fold')
            ],
            'editing.notes'            => [

            ]

        ];
    }

    public function mount()
    {
        $this->editing = $this->makeBlankAluminiumShutter();
    }

    private function makeBlankAluminiumShutter()
    {
        return AluminiumShutter::make([
            'shutter_type' => '',
            'corner' => '',
            'panel_layout' => '',
            'in_or_out' => '',
            'mid_rail' => '',
            'mid_rail_height' => '',
            'blade_size' => '',
            'tilt_rod' => '',
            'colour' => '',
            'frame' => '',
            'notes' => ''
        ]);
    }

    public function addAluminiumShutter()
    {
        $this->emitUp('productAdded', 'aluminium_shutters', $this->editing);
    }

    public function setProductData($productData = [])
    {
        if ($productData) {
            $this->editing = AluminiumShutter::make($productData);
        }

        $this->resetErrorBag();
    }

    public function render()
    {
        return view('livewire.aluminium-shutter-form');
    }
}
