<?php

namespace App\Http\Livewire\DataTable;

trait WithEmailForm
{

    public string $subject = 'Quote [Quote Number] from [Trading Name] for [Customer Name]';
    public string $content = "Hi [Customer Name],

Thank you for your enquiry.

Here's quote [Quote Number] for [Currency Code] [Quote Total Without Currency].

View your quote online: [Online Quote Link]

From your online quote you can accept, decline, comment or print.

If you have any questions, please let us know.

Thanks,
[Trading Name]

Terms
[Terms]";

    public function emailForm()
    {
        $this->showEmailModal = true;
    }
    
    public function sendEmail()
    {
        $this->emitTo('email-form', 'sendEmail', [
            'modelClass' => $this->getModelClass(),
            'id' => current($this->selected)
        ]);
    }

    private function canSendEmail(): bool
    {
        return count($this->selected) == 1;
    }

    abstract public function getModelClass(): string;
}
