<?php

namespace App\Http\Livewire;

use App\Models\AuPvcShutter;
use Illuminate\Validation\Rule;
use Livewire\Component;

class AuPvcShutterForm extends Component
{
    public AuPvcShutter $editing;

    protected $listeners = [
        'addProduct' => 'addAuPvcShutter',
        'setProductData' => 'setProductData'

    ];

    public function rules()
    {
        return [
            'editing.name'            => 'required|max:10',
            'editing.width'           => 'required|integer|min:250',
            'editing.drop'            => 'required|integer|max:2440|min:250',
            'editing.shutter_type' => [
                'required',
                Rule::in(
                    'Standard',
                    'Sliding',
                    'Bifold Track',
                    'U Channel'
                )
            ],
            'editing.corner' => [
                'required',
                Rule::in('No', '90', '135')
            ],
            'editing.panel_layout' => [
                'required',
                'regex:/^[C|D|L|R|T|-]+$/i'
            ],
            'editing.in_or_out' => [
                'required',
                Rule::in('In', 'Out')
            ],
            'editing.mid_rail'        => [
                'required',
                Rule::in('NA', 'Centre', '1', '2')
            ],
            'editing.mid_rail_height' => [

            ],
            'editing.blade_size'      => [
                'required',
                Rule::in('89mm')
            ],
            'editing.tilt_rod'        => [
                'required',
                Rule::in('Clear View')
            ],
            'editing.colour'          => [
                'required',
                Rule::in(
                    'White',
                    'Off White',
                    'Custom'
                )
            ],
            'editing.stile_type'      => [
                'required',
                Rule::in('50b')
            ],
            'editing.frame'           => [
                'required',
                Rule::in('No Frame', 'Z20-C6', 'BL50-B10', 'BL65-B10A')
            ],
            'editing.frame_options'   => [
                'required',
                Rule::in('NA', 'LRTB', 'LRT', 'LRB')
            ],
            'editing.hinge_type'      => [
                'required',
                Rule::in('NA', 'Non Mortised', 'Pivot')
            ],
            'editing.hinge_colour'    => [
                'required',
                Rule::in('White')
            ],
            'editing.notes'            => [

            ]
        ];
    }

    public function mount()
    {
        $this->editing = $this->makeBlankAuPvcShutterForm();
    }

    private function makeBlankAuPvcShutterForm()
    {
        return AuPvcShutter::make([
            'shutter_type' => '',
            'corner' => '',
            'panel_layout' => '',
            'in_or_out' => '',
            'mid_rail' => '',
            'mid_rail_height' => '',
            'blade_size' => '',
            'tilt_rod' => '',
            'stile_type' => '',
            'colour' => '',
            'frame' => '',
            'frame_options' => '',
            'hinge_type' => '',
            'hinge_colour' => '',
            'notes' => ''
        ]);
    }

    public function addAuPvcShutter()
    {
        $this->validate();

        $this->emitUp('productAdded', 'au_pvc_shutters', $this->editing);
    }

    public function setProductData($productData = null)
    {
        if ($productData) {
            $this->editing = AuPvcShutter::make($productData);
        }

        $this->resetErrorBag();
    }

    public function render()
    {
        return view('livewire.au-pvc-shutter-form');
    }


}
