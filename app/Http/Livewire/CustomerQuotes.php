<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithEmailForm;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Http\Livewire\DataTable\WithSorting;
use App\Models\CustomerQuote;
use App\Models\Order;
use App\Models\Quote;
use Livewire\Component;

class CustomerQuotes extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows, WithEmailForm;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;
    public bool $showExtraItemsModel = false;
    public bool $showEmailModal = false;

    public CustomerQuote $editing;


    public array $filters = [
        'search'     => '',
    ];

    protected array $rules = [
        'editing.markup_type' => 'required',
        'editing.markup_value' => 'required|numeric|gt:0',
    ];

    protected $listeners = ['emailSent'];

    public function emailForm()
    {
        $this->showEmailModal = true;
    }

    public function emailSent()
    {
        $this->showEmailModal = false;
    }

    public function edit(CustomerQuote $customerQuote)
    {
        $this->useCachedRows();

        $this->editing = $customerQuote;

        $this->showEditModal = true;
    }

    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->delete();

        $this->showDeleteModal = false;

        $this->notify('You\'ve deleted '.$deleteCount.' rows');
    }

    public function extraItems(CustomerQuote $customerQuote)
    {
        $this->useCachedRows();

        $this->editing = $customerQuote;

        $this->showExtraItemsModel = true;
    }

    public function saveExtraItems()
    {
        $this->emitTo('extra-items-form', 'saveItems');
        $this->showExtraItemsModel = false;
    }

    public function getModelClass(): string
    {
        return CustomerQuote::class;
    }

    /**
     * @throws \Exception
     */
    public function submitOrder()
    {
        foreach ($this->selected as $selected) {
            $customerQuote = CustomerQuote::find($selected);
            Order::createFromQuote($customerQuote->quote);
        }

        $this->notify('Order Submitted!');
    }


    public function getRowsQueryProperty()
    {
        $query = Quote::with('tenant')->with('customerQuote')
                ->select('quotes.*')
                ->join('tenants', 'tenants.id', '=', 'quotes.tenant_id')
                ->join('customer_quotes', 'customer_quotes.quote_id', '=', 'quotes.id')
                ->whereDoesntHave('convertedQuotes')
                ->whereHas('customerQuote')
//            ->when($this->filters['status'], fn($query, $status) => $query->where('status', $status))
//            ->when($this->filters['amount-min'], fn($query, $amount) => $query->where('amount', '>=', $amount))
//            ->when($this->filters['amount-max'], fn($query, $amount) => $query->where('amount', '<=', $amount))
//            ->when($this->filters['date-min'], fn($query, $date) => $query->where('date', '>=', Carbon::parse($date)))
//            ->when($this->filters['date-max'], fn($query, $date) => $query->where('date', '<=', Carbon::parse($date)))
                ->when($this->filters['search'], fn($query, $search) => $query->where('po_reference', 'like', '%' . $search . '%')
                    ->orWhere('tenants.name', 'like', '%' . $search . '%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.customer-quotes', [
            'quotes' => $this->rows
        ]);
    }
}
