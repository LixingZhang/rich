<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithEmailForm;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Quote;
use Livewire\Component;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Livewire\DataTable\WithSorting;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Models\CustomerQuote;

class Quotes extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows, WithEmailForm;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;
    public bool $showEmailModal = false;

    public ?CustomerQuote $customerQuote = null;

    public ?Quote $quote = null;

    public ?Customer $customer = null;

    public bool $addNewCustomer = false;

    public array $filters = [
        'search'     => '',
        'status'     => '',
        'tenant'     => '',
        'amount-min' => null,
        'amount-max' => null,
        'date-min'   => null,
        'date-max'   => null,
    ];

    public function rules()
    {
        return [
            'customer.tenant_id' => [
                'integer'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.name' => [
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.email' => [
                'string',
                'email',
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.contact_name' => [
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.address' => [
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.city' => [
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.postcode' =>[
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customer.state' => [
                'string'
            ] + ($this->addNewCustomer ? ['required'] : ['nullable']),
            'customerQuote.customer_id' => [
                'numeric'
            ] + ($this->addNewCustomer ? ['nullable'] : ['required']),
            'customerQuote.markup_type' => 'required',
            'customerQuote.markup_value' => 'required|numeric',
        ];
    }

    public function submitOrder()
    {
        foreach ($this->selected as $selected) {
            $quote = Quote::find($selected);
            Order::createFromQuote($quote);
        }

        $this->notify('Order Submitted!');
    }

    public function updatedFilters()
    {
        $this->resetPage();
    }

    public function resetFilters()
    {
        $this->reset('filters');
    }

    public function edit(Quote $quote)
    {
        $this->useCachedRows();

        $this->quote = $quote;
        $this->customer = Customer::make(['state' => '', 'tenant_id' => '']);
        $this->customerQuote = CustomerQuote::make(['customer_id' => '', 'markup_type' => '']);

        $this->showEditModal = true;
    }
    public function cancel()
    {
        $this->showEditModal = false;

        $this->reset();
    }

    public function getModelClass(): string
    {
        return Quote::class;
    }

    public function createCustomerQuote()
    {
        $this->validate();

        if ($this->addNewCustomer) {
            $this->customer->save();
            $this->customerQuote->customer()->associate($this->customer);
        }

        $this->customerQuote->quote()->associate($this->quote);
        $this->customerQuote->save();

        $this->showEditModal = false;

        $this->notify('Customer Quote Created');

        $this->reset();

    }


    public function getRowsQueryProperty(): Builder
    {
        $query = Quote::with('tenant')->with('staff')
            ->select('quotes.*')
            ->join('tenants', 'tenants.id', '=', 'quotes.tenant_id')
            ->join('users', 'users.id', '=', 'quotes.staff_id')
            ->whereDoesntHave('convertedQuotes')
            ->whereDoesntHave('customerQuote')
            ->when($this->filters['search'], fn ($query, $search) => $query->where('po_reference', 'like', '%' . $search . '%')
                ->orWhere('tenants.name', 'like', '%' . $search . '%'));

        if (empty($this->sorts)) {
            $query->orderByDesc('created_at');
        }

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.quotes', [
            'quotes' => $this->rows,
            'canSendEmail' => $this->canSendEmail()
        ]);
    }
}
