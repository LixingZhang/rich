<?php

namespace App\Http\Livewire;

use App\Models\RollerBlind;
use Illuminate\Validation\Rule;
use Livewire\Component;

class RollerBlindForm extends Component
{
    public RollerBlind $editing;

    protected $listeners = [
        'addProduct' => 'addRollerBlind',
        'setProductData' => 'setProductData'
    ];


    public function rules()
    {
        return [
            'editing.name'             => 'required|max:10',
            'editing.width'            => 'required|integer|max:3010',
            'editing.drop'             => 'required|integer|max:3300',
            'editing.type'             => [
                'required',
                Rule::in(['Blockout', 'Screen', 'Light Filter'])
            ],
            'editing.fabric'           => [
                'required'
            ],
            'editing.colour'           => [
                'required'
            ],
            'editing.control_type'     => [
                'required',
                Rule::in('Chain', 'Motor')
            ],
            'editing.control_side'     => [
                'required',
                Rule::in('Left', 'Right')
            ],
            'editing.chain_length'     => [
                'numeric',
            ],
            'editing.component_colour' => [
                'required',
                Rule::in(
                    'White',
                    'Black'
                )
            ],
            'editing.base_rail'        => [
                'required',
                Rule::in(
                    'Silver',
                    'Black'
                )
            ],
            'editing.roll_direction'   => [
                'required',
                Rule::in(
                    'Standard',
                    'Reverse'
                )
            ],
            'editing.motor_type'       => [
                'required',
                Rule::in(
                    'NA',
                    'Acmeda 240v',
                    'Acmeda Li Ion 1.1nm',
                    'Acmeda Li Ion 3.0nm'
                )
            ],
            'editing.charger'          => [
                'required',
                Rule::in(
                    'NA',
                    'Yes'
                )
            ],
            'editing.wifi_hub'         => [
                'required',
                Rule::in(
                    'NA',
                    'Acmeda Pulse'
                )
            ],
            'editing.remote'           => [
                'required',
                Rule::in(
                    'NA',
                    '1 Channel',
                    '15 Channel'
                )
            ],
            'editing.bracket_options'   => [
                'required',
                Rule::in(
                    'Single',
                    'Slim Combo Top Front',
                    'Slim Combo Top Back',
                    'Double Bracket'
                )
            ],
            'editing.notes'            => [

            ]
        ];
    }

    public function mount()
    {
        $this->editing = $this->makeBlankRollerBlind();
    }

//    public function updatedEditingWidth()
//    {
//        $this->editing->sqm = (int) $this->editing->width * (int) $this->editing->drop;
//    }
//
//    public function updatedEditingDrop()
//    {
//        $this->editing->sqm = (int) $this->editing->width * (int) $this->editing->drop;
//    }

    public function makeBlankRollerBlind()
    {
        return RollerBlind::make([
            'type' => '',
            'fabric' => '',
            'colour' => '',
            'control_type' => '',
            'control_side' => '',
            'component_colour' => '',
            'base_rail' => '',
            'roll_direction' => '',
            'motor_type' => '',
            'charger' => '',
            'wifi_hub' => '',
            'remote' => '',
            'bracket_options' => ''
        ]);
    }

    public function addRollerBlind()
    {
        $this->validate();

        $this->emitUp('productAdded', 'roller_blinds', $this->editing);
    }

    public function setProductData($product = null)
    {
        if ($product) {
            $this->editing = RollerBlind::make($product);
        }

        $this->resetErrorBag();
    }

    public function render()
    {
        return view('livewire.roller-blind-form');
    }
}
