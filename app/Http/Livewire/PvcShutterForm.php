<?php

namespace App\Http\Livewire;

use App\Models\PvcShutter;
use Illuminate\Validation\Rule;
use Livewire\Component;

class PvcShutterForm extends Component
{
    public PvcShutter $editing;

    protected $listeners = [
        'addProduct' => 'addPvcShutter',
        'setProductData' => 'setProductData'
    ];

    public function rules()
    {
        return [
            'editing.width'           => 'required|integer|min:300',
            'editing.drop'            => 'required|integer|min:300',
            'editing.name' => 'required|max:255',
            'editing.shutter_type' => [
                'required',
                Rule::in(
                    'Standard',
                    'Sliding',
                    'Bifold Track',
                    'U Channel'
                )
            ],
            'editing.corner' => [
                'required',
                Rule::in('No', '90', '135')
            ],
            'editing.panel_layout' => [
                'required',
                'regex:/^[C|D|L|R|T|-]+$/i'
            ],
            'editing.in_or_out' => [
                'required',
                Rule::in('In', 'Out')
            ],
            'editing.mid_rail'        => [
                'required',
                Rule::in('NA', 'Centre', '1', '2')
            ],
            'editing.mid_rail_height' => [

            ],
            'editing.blade_size'      => [
                'required',
                Rule::in('89mm', '64mm')
            ],
            'editing.tilt_rod'        => [
                'required',
                Rule::in('Clear View', 'Central')
            ],
            'editing.colour'          => [
                'required',
                Rule::in('W100 Snow', 'W101 Dove', 'W104 China', 'W105 Soft Pearl', 'W107 Pearl', 'W109 Linen Wash',
                    'W115 Clay', 'W118 Wheat', 'W401 Eggshell', 'W402 Palm Beach', 'W00 Custom')
            ],
            'editing.stile_type'      => [
                'required',
                Rule::in('50b')
            ],
            'editing.frame'           => [
                'required',
                Rule::in('No Frame', 'Z20-C6', 'BL50-B10', 'BL65-B10A')
            ],
            'editing.frame_options'   => [
                'required',
                Rule::in('NA', 'LRTB', 'LRT', 'LRB')
            ],
            'editing.hinge_type'      => [
                'required',
                Rule::in('NA', 'Non Mortised', 'Pivot')
            ],
            'editing.hinge_colour'    => [
                'required',
                Rule::in('White', 'Nickel', 'Stainless', 'Colour Match')
            ],
            'editing.notes'            => [

            ]
        ];
    }

    public function mount()
    {
        $this->editing = $this->makeBlankPvcShutterForm();
    }

    private function makeBlankPvcShutterForm()
    {
        return PvcShutter::make([
            'shutter_type'    => '',
            'corner'          => '',
            'panel_layout'    => '',
            'in_or_out'       => '',
            'mid_rail'        => '',
            'mid_rail_height' => '',
            'blade_size'      => '',
            'tilt_rod'        => '',
            'stile_type'      => '',
            'colour'          => '',
            'frame'           => '',
            'frame_options'   => '',
            'hinge_type'      => '',
            'hinge_colour'    => '',
            'notes'           => ''
        ]);
    }

    public function addPvcShutter()
    {
        $this->validate();

        $this->emitUp('productAdded', 'pvc_shutters', $this->editing);
    }

    public function setProductData($productData = null)
    {
        if ($productData) {
            $this->editing = PvcShutter::make($productData);
        }

        $this->resetErrorBag();
    }

    public function render()
    {
        return view('livewire.pvc-shutter-form');
    }
}
