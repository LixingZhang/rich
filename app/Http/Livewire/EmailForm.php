<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Livewire\Component;

class EmailForm extends Component
{
    public string $subject = '';
    public string $content = '';

    protected $listeners = ['sendEmail'];

    protected array $rules = [
        'subject' => 'required',
        'content' => 'required'
    ];

    public function sendEmail($data)
    {
        $this->validate();

        $object = $data['modelClass']::find($data['id']);
        $pdf = \PDF::loadView('pdf.customer-quote', ['customerQuote' => $object]);

        $this->renderVariables($object);

        Mail::raw($this->content, function ($message) use ($pdf, $object) {
            $message->to($object->customer->email)
                ->subject($this->subject)
                ->attachData($pdf->output(), $object->po_reference.'.pdf', [
                    'mime' => 'application/pdf',
                ]);
        });

        $this->notify('Email Sent Successfully');
    }


    private function renderVariables($object)
    {
        $search = [
            '[Quote Number]',
            '[Trading Name]',
            '[Customer Name]',
            '[Online Quote Link]',
            '[Currency Code]',
            '[Quote Total Without Currency]'
        ];

        $replace = [
            $object->po_reference,
            $object->tenant->name,
            $object->customer_name,
            URL::signedRoute('customer-quotes.show', ['customerQuote' => $object->id]),
            'AUD',
            $object->amount
        ];

        $this->content = str_replace($search, $replace, $this->content);
        $this->subject = str_replace($search, $replace, $this->subject);

        $this->emitUp('emailSent');
    }

    public function render()
    {
        return view('livewire.email-form');
    }
}
