<?php

namespace App\Http\Livewire;

use App\Mail\OrderShipped;
use App\Models\AluminiumShutter;
use App\Models\AuPvcShutter;
use App\Models\BasswoodShutter;
use App\Models\Order;
use App\Models\ProductDiscount;
use App\Models\PvcShutter;
use App\Models\Quote;
use App\Models\QuoteItem;
use App\Models\RollerBlind;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Dashboard extends Component
{
    public bool $showEditModal = false;
    public bool $showConfirmModal = false;
    public bool $showDuplicateModal = false;

    public float $sum = 0.00;

    public string $currentProduct = 'Basswood Shutters';

    public User $user;

    public array $form =  [
        'tenant' => '',
        'reference' => '',
        'notes' => '',
        'basswood_shutters' => [],
        'pvc_shutters' => [],
        'au_pvc_shutters' => [],
        'aluminium_shutters' => [],
        'roller_blinds' => []
    ];

    protected array $rules = [
        'form.tenant' => 'required',
        'form.reference' => 'required',
        'form.notes' => 'max:10'
    ];

    public array $productHeaders = [
        [
            'name'    => 'Basswood Shutters',
            'key'     => 'basswood_shutters',
            'headers' => [
                ['text' => 'Name', 'value' => 'name', 'sortable' => false],
                ['text' => 'Width(mm)', 'value' => 'width', 'sortable' => false],
                ['text' => 'Drop(mm)', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Sqm', 'value' => 'sqm', 'sortable' => false],
                ['text' => 'Shutter Type', 'value' => 'shutter_type', 'sortable' => false],
                ['text' => 'Corner', 'value' => 'corner', 'sortable' => false],
                ['text' => 'Panel Layout', 'value' => 'panel_layout', 'sortable' => false],
                ['text' => 'Panel Qty', 'value' => 'panel_qty', 'sortable' => false],
                ['text' => 'In or Out', 'value' => 'in_or_out', 'sortable' => false],
                ['text' => 'Mid Rail', 'value' => 'mid_rail', 'sortable' => false],
                ['text' => 'Mid Rail Height', 'value' => 'mid_rail_height', 'sortable' => false],
                ['text' => 'Blade Size', 'value' => 'blade_size', 'sortable' => false],
                ['text' => 'Tilt Rod', 'value' => 'tilt_rod', 'sortable' => false],
                ['text' => 'Colour', 'value' => 'colour', 'sortable' => false],
                ['text' => 'Stile Type', 'value' => 'stile_type', 'sortable' => false],
                ['text' => 'Frame', 'value' => 'frame', 'sortable' => false],
                ['text' => 'Frame Options', 'value' => 'frame_options', 'sortable' => false],
                ['text' => 'Hinge Type', 'value' => 'hinge_type', 'sortable' => false],
                ['text' => 'Hinge Colour', 'value' => 'hinge_colour', 'sortable' => false],
                ['text' => 'Notes', 'value' => 'notes', 'sortable' => false],
                ['text' => 'Actions', 'value' => 'actions', 'sortable' => false]
            ]
        ],
        [
            'name'    => 'PVC Shutters',
            'key'     => 'pvc_shutters',
            'headers' => [
                ['text' => 'Name', 'value' => 'name', 'sortable' => false],
                ['text' => 'Width(mm)', 'value' => 'width', 'sortable' => false],
                ['text' => 'Drop(mm)', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Sqm', 'value' => 'sqm', 'sortable' => false],
                ['text' => 'Shutter Type', 'value' => 'shutter_type', 'sortable' => false],
                ['text' => 'Corner', 'value' => 'corner', 'sortable' => false],
                ['text' => 'Panel Layout', 'value' => 'panel_layout', 'sortable' => false],
                ['text' => 'Panel Qty', 'value' => 'panel_qty', 'sortable' => false],
                ['text' => 'In or Out', 'value' => 'in_or_out', 'sortable' => false],
                ['text' => 'Mid Rail', 'value' => 'mid_rail', 'sortable' => false],
                ['text' => 'Mid Rail Height', 'value' => 'mid_rail_height', 'sortable' => false],
                ['text' => 'Blade Size', 'value' => 'blade_size', 'sortable' => false],
                ['text' => 'Tilt Rod', 'value' => 'tilt_rod', 'sortable' => false],
                ['text' => 'Stile Type', 'value' => 'stile_type', 'sortable' => false],
                ['text' => 'Colour', 'value' => 'colour', 'sortable' => false],
                ['text' => 'Frame', 'value' => 'frame', 'sortable' => false],
                ['text' => 'Frame Options', 'value' => 'frame_options', 'sortable' => false],
                ['text' => 'Hinge Type', 'value' => 'hinge_type', 'sortable' => false],
                ['text' => 'Hinge Colour', 'value' => 'hinge_colour', 'sortable' => false],
                ['text' => 'Notes', 'value' => 'notes', 'sortable' => false],
                ['text' => 'Actions', 'value' => 'actions', 'sortable' => false]
            ]
        ],
        [
            'name'    => 'AU PVC Shutters',
            'key'     => 'au_pvc_shutters',
            'headers' => [
                ['text' => 'Name', 'value' => 'name', 'sortable' => false],
                ['text' => 'Width(mm)', 'value' => 'width', 'sortable' => false],
                ['text' => 'Drop(mm)', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Sqm', 'value' => 'sqm', 'sortable' => false],
                ['text' => 'Shutter Type', 'value' => 'shutter_type', 'sortable' => false],
                ['text' => 'Corner', 'value' => 'corner', 'sortable' => false],
                ['text' => 'Panel Layout', 'value' => 'panel_layout', 'sortable' => false],
                ['text' => 'Panel Qty', 'value' => 'panel_qty', 'sortable' => false],
                ['text' => 'In or Out', 'value' => 'in_or_out', 'sortable' => false],
                ['text' => 'Mid Rail', 'value' => 'mid_rail', 'sortable' => false],
                ['text' => 'Mid Rail Height', 'value' => 'mid_rail_height', 'sortable' => false],
                ['text' => 'Blade Size', 'value' => 'blade_size', 'sortable' => false],
                ['text' => 'Tilt Rod', 'value' => 'tilt_rod', 'sortable' => false],
                ['text' => 'Stile Type', 'value' => 'stile_type', 'sortable' => false],
                ['text' => 'Colour', 'value' => 'colour', 'sortable' => false],
                ['text' => 'Frame', 'value' => 'frame', 'sortable' => false],
                ['text' => 'Frame Options', 'value' => 'frame_options', 'sortable' => false],
                ['text' => 'Hinge Type', 'value' => 'hinge_type', 'sortable' => false],
                ['text' => 'Hinge Colour', 'value' => 'hinge_colour', 'sortable' => false],
                ['text' => 'Notes', 'value' => 'notes', 'sortable' => false],
                ['text' => 'Actions', 'value' => 'actions', 'sortable' => false]
            ]
        ],
        [
            'name'    => 'Aluminium Shutters',
            'key'     => 'aluminium_shutters',
            'headers' => [
                ['text' => 'Name', 'value' => 'name', 'sortable' => false],
                ['text' => 'Width(mm)', 'value' => 'width', 'sortable' => false],
                ['text' => 'Drop(mm)', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Sqm', 'value' => 'sqm', 'sortable' => false],
                ['text' => 'Shutter Type', 'value' => 'shutter_type', 'sortable' => false],
                ['text' => 'Corner', 'value' => 'corner', 'sortable' => false],
                ['text' => 'Panel Layout', 'value' => 'panel_layout', 'sortable' => false],
                ['text' => 'Panel Qty', 'value' => 'panel_qty', 'sortable' => false],
                ['text' => 'In or Out', 'value' => 'in_or_out', 'sortable' => false],
                ['text' => 'Mid Rail', 'value' => 'mid_rail', 'sortable' => false],
                ['text' => 'Mid Rail Height', 'value' => 'mid_rail_height', 'sortable' => false],
                ['text' => 'Blade Size', 'value' => 'blade_size', 'sortable' => false],
                ['text' => 'Tilt Rod', 'value' => 'tilt_rod', 'sortable' => false],
                ['text' => 'Colour', 'value' => 'colour', 'sortable' => false],
                ['text' => 'Frame', 'value' => 'frame', 'sortable' => false],
                ['text' => 'Notes', 'value' => 'notes', 'sortable' => false],
                ['text' => 'Actions', 'value' => 'actions', 'sortable' => false]
            ]
        ],
        [
            'name'    => 'Roller Blinds',
            'key'     => 'roller_blinds',
            'headers' => [
                ['text' => 'Name', 'value' => 'name', 'sortable' => false],
                ['text' => 'Width(mm)', 'value' => 'width', 'sortable' => false],
                ['text' => 'Drop(mm)', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Sqm', 'value' => 'drop', 'sortable' => false],
                ['text' => 'Type', 'value' => 'type', 'sortable' => false],
                ['text' => 'Fabric', 'value' => 'fabric', 'sortable' => false],
                ['text' => 'Colour', 'value' => 'colour', 'sortable' => false],
                ['text' => 'Control Type', 'value' => 'control_type', 'sortable' => false],
                ['text' => 'Control Side', 'value' => 'control_side', 'sortable' => false],
                ['text' => 'Chain Length', 'value' => 'chain_length', 'sortable' => false],
                ['text' => 'Component Colour', 'value' => 'component_colour', 'sortable' => false],
                ['text' => 'Base Rail', 'value' => 'base_rail', 'sortable' => false],
                ['text' => 'Roll Direction', 'value' => 'roll_direction', 'sortable' => false],
                ['text' => 'Motor Type', 'value' => 'motor_type', 'sortable' => false],
                ['text' => 'Charger', 'value' => 'charger', 'sortable' => false],
                ['text' => 'Wifi Hub', 'value' => 'wifi_hub', 'sortable' => false],
                ['text' => 'Remote', 'value' => 'remote', 'sortable' => false],
                ['text' => 'Bracket Options', 'value' => 'bracket_options', 'sortable' => false],
                ['text' => 'Notes', 'value' => 'notes', 'sortable' => false],
                ['text' => 'Actions', 'value' => 'actions', 'sortable' => false]
            ]
        ]
    ];

    protected $listeners = ['productAdded' => 'productAdded'];

    public function mount()
    {
        $this->user = auth()->user();

        if (session()->has($this->sessionFormKey())) {
            $this->form = session($this->sessionFormKey());
        }
    }

    public function updatedCurrentProduct($value)
    {
        session()->put($this->sessionProductKey(), $value);
    }

    public function confirm($action)
    {
        $this->showConfirmModal = true;
    }

    /**
     * @throws \Exception
     */
    public function submitOrder()
    {
        $this->validate();

        $tenant = Tenant::find((int) $this->form['tenant']);

        Order::createFromFormData(
            $tenant,
            $this->form['basswood_shutters'],
            BasswoodShutter::class,
            $this->form['reference'],
            $this->form['notes']
        );
        Order::createFromFormData(
            $tenant,
            $this->form['pvc_shutters'],
            PvcShutter::class,
            $this->form['reference'],
            $this->form['notes']
        );
        Order::createFromFormData(
            $tenant,
            $this->form['au_pvc_shutters'],
            AuPvcShutter::class,
            $this->form['reference'],
            $this->form['notes']
        );
        Order::createFromFormData(
            $tenant,
            $this->form['aluminium_shutters'],
            AluminiumShutter::class,
            $this->form['reference'],
            $this->form['notes']
        );
        Order::createFromFormData(
            $tenant,
            $this->form['roller_blinds'],
            RollerBlind::class,
            $this->form['reference'],
            $this->form['notes']
        );

        $this->resetForm();

        $this->notify('Order Submitted');
    }

    public function submitQuote()
    {
        $this->validate();

        $quote = Quote::create([
            'tenant_id'     => $this->form['tenant'],
            'reference'  => $this->form['reference'],
            'notes'         => $this->form['notes'],
        ]);

        $items = [];
        foreach ($this->form['basswood_shutters'] as $shutter) {
            $item = QuoteItem::createFromProduct(BasswoodShutter::create($shutter));
            $quote->amount += $item->discounted_price;
            $items[] = $item;
        }

        foreach ($this->form['pvc_shutters'] as $shutter) {
            $item = QuoteItem::createFromProduct(PvcShutter::create($shutter));
            $quote->amount += $item->discounted_price;
            $items[] = $item;
        }

        foreach ($this->form['au_pvc_shutters'] as $shutter) {
            $item = QuoteItem::createFromProduct(AuPvcShutter::create($shutter));
            $quote->amount += $item->discounted_price;
            $items[] = $item;
        }

        foreach ($this->form['aluminium_shutters'] as $shutter) {
            $item = QuoteItem::createFromProduct(AluminiumShutter::create($shutter));
            $quote->amount += $item->discounted_price;
            $items[] = $item;
        }

        foreach ($this->form['roller_blinds'] as $rollBlind) {
            $item = QuoteItem::createFromProduct(RollerBlind::create($rollBlind));
            $quote->amount += $item->discounted_price;
            $items[] = $item;
        }

        $quote->staff()->associate(auth()->user());
        $quote->items()->saveMany($items);
        $quote->save();

        $this->resetForm();

        $this->notify('Quote Submitted');
    }

    public function resetForm()
    {
        if (session()->has($this->sessionFormKey())) {
            session()->forget($this->sessionFormKey());
        }

        $this->reset('form');
        $this->resetValidation();
    }

    public function addProduct()
    {
        $this->emit('addProduct');
    }

    public function showProductModal()
    {
        $this->showEditModal = true;

        $this->emit('setProductData');
    }

    public function updatedForm()
    {
        session()->put($this->sessionFormKey(), $this->form);
        $basePrice = 0;
        foreach ($this->form['roller_blinds'] as $rollBlind) {
            $rollBlind = RollerBlind::make($rollBlind);
            $basePrice += $rollBlind->calculatePrice();
        }

        $this->basePrice = $basePrice;
    }

    public function productAdded($productType, $productData)
    {
        $this->showEditModal = false;

        $this->form[$productType][] = $productData;

        session()->put($this->sessionFormKey(), $this->form);
    }

    public function delete($currentProduct, $index)
    {
        array_splice($this->form[$currentProduct], $index, 1);

        session()->put($this->sessionFormKey(), $this->form);
    }

    public function duplicate($currentProduct, $index)
    {
        $this->showEditModal = true;

        $this->emit('setProductData', $this->form[$currentProduct][$index]);
    }

    public function render()
    {
//        Mail::to('zlxjackie@hotmail.com')->send(new OrderShipped());
        return view('livewire.dashboard', [
            'canSubmit' => $this->canSubmit(),
            'pricing' => $this->pricing,
        ]);
    }

    private function canSubmit()
    {
        return $this->form['basswood_shutters'] || $this->form['roller_blinds']
            || $this->form['pvc_shutters'] || $this->form['au_pvc_shutters'] || $this->form['aluminium_shutters'];
    }

    private function sessionFormKey()
    {
        return sprintf('user_%s_form', $this->user->id);
    }

    private function sessionProductKey()
    {
        return sprintf('user_%s_product', $this->user->id);
    }

    public function getPricingProperty()
    {
        $pricing = [];
        $this->sum = 0.00;

        $initialValues = [
            'base_price' => 0,
            'discount' => '',
            'discounted_price' => 0
        ];
        if (!empty($this->form['basswood_shutters'])) {
            $pricing[BasswoodShutter::NAME] = $initialValues;
            $discount = ProductDiscount::where('product', BasswoodShutter::NAME)->first();
            foreach ($this->form['basswood_shutters'] as $shutter) {
                $shutter = BasswoodShutter::make($shutter);
                $basePrice = $shutter->calculatePrice();
                $pricing[$shutter->getName()]['base_price'] += $basePrice;
                if ($discount) {
                    $pricing[$shutter->getName()]['discount'] = $discount->discount;
                    $pricing[$shutter->getName()]['discounted_price'] += $basePrice * (1 - $discount->discount / 100);
                    $this->sum += $basePrice * (1 - $discount->discount / 100);
                } else {
                    $this->sum += $basePrice;
                }
            }
        }

        if (!empty($this->form['au_pvc_shutters'])) {
            $pricing[AuPvcShutter::NAME] = $initialValues;
            $discount = ProductDiscount::where('product', AuPvcShutter::NAME)->first();
            foreach ($this->form['au_pvc_shutters'] as $shutter) {
                $shutter = AuPvcShutter::make($shutter);
                $basePrice = $shutter->calculatePrice();
                $pricing[$shutter->getName()]['base_price'] += $basePrice;
                if ($discount) {
                    $pricing[$shutter->getName()]['discount'] = $discount->discount;
                    $pricing[$shutter->getName()]['discounted_price'] += $basePrice * (1 - $discount->discount / 100);
                    $this->sum += $basePrice * (1 - $discount->discount / 100);
                } else {
                    $this->sum += $basePrice;
                }
            }
        }

        if (!empty($this->form['pvc_shutters'])) {
            $pricing[PvcShutter::NAME] = $initialValues;
            $discount = ProductDiscount::where('product', PvcShutter::NAME)->first();
            foreach ($this->form['pvc_shutters'] as $shutter) {
                $shutter = PvcShutter::make($shutter);
                $basePrice = $shutter->calculatePrice();
                $pricing[$shutter->getName()]['base_price'] += $basePrice;
                if ($discount) {
                    $pricing[$shutter->getName()]['discount'] = $discount->discount;
                    $pricing[$shutter->getName()]['discounted_price'] += $basePrice * (1 - $discount->discount / 100);;
                    $this->sum += $basePrice * (1 - $discount->discount / 100);
                } else {
                    $this->sum += $basePrice;
                }
            }
        }

        if (!empty($this->form['aluminium_shutters'])) {
            $pricing[AluminiumShutter::NAME] = $initialValues;
            $discount = ProductDiscount::where('product', AluminiumShutter::NAME)->first();
            foreach ($this->form['aluminium_shutters'] as $shutter) {
                $shutter = AluminiumShutter::make($shutter);
                $basePrice = $shutter->calculatePrice();
                $pricing[$shutter->getName()]['base_price'] += $basePrice;
                if ($discount) {
                    $pricing[$shutter->getName()]['discount'] = $discount->discount;
                    $pricing[$shutter->getName()]['discounted_price'] += $basePrice * (1 - $discount->discount / 100);
                    $this->sum += $basePrice * (1 - $discount->discount / 100);
                } else {
                    $this->sum += $basePrice;
                }
            }
        }

        if (!empty($this->form['roller_blinds'])) {
            $pricing[RollerBlind::NAME] = $initialValues;
            $discount = ProductDiscount::where('product', RollerBlind::NAME)->first();
            foreach ($this->form['roller_blinds'] as $rollBlind) {
                $rollBlind = RollerBlind::make($rollBlind);
                $basePrice = $rollBlind->calculatePrice();
                $pricing[$rollBlind->getName()]['base_price'] += $basePrice;
                if ($discount) {
                    $pricing[$rollBlind->getName()]['discount'] = $discount->discount;
                    $pricing[$rollBlind->getName()]['discounted_price'] += $basePrice * (1 - $discount->discount / 100);
                    $this->sum += $basePrice * (1 - $discount->discount / 100);
                } else {
                    $this->sum += $basePrice;
                }
            }
        }


        return $pricing;
    }
}
