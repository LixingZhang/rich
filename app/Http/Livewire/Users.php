<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Http\Livewire\DataTable\WithSorting;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Users extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;

    public User $editing;

    public array $tenantIds = [];

    public array $filters = [
        'search' => '',
    ];

    protected $rules = [
        'tenantIds' => 'required',
        'editing.name' => 'required',
        'editing.email' => 'email|required|unique:users,email',
        'editing.role' => 'required',
    ];

    public function create()
    {
        Gate::authorize('create', User::class);

        $this->useCachedRows();

        $this->editing = User::make();

        $this->showEditModal = true;

        $this->resetValidation();
    }

    public function save()
    {
        Gate::authorize('create', User::class);

        $this->validate();

        $this->editing->save();

        $this->editing->tenants()->attach($this->tenantIds);

        $this->showEditModal = false;

        $this->notify('New user created');
    }

    public function getRowsQueryProperty()
    {
        $query = User::with('tenants')
            ->whereHas(
                'tenants',
                function ($query) {
                    $query
                        ->when(
                            auth()->user()->isTenant(),
                            fn($query) => $query->whereIn('tenants.id', auth()->user()->tenants->pluck('id'))
                        )->when(
                            $this->filters['search'],
                            fn($query, $search) => $query->where('tenants.name', 'like', '%' . $search . '%')
                                ->orWhere('users.name', 'like', '%' . $search . '%'));
                }
            );

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.users', [
            'users' => $this->rows
        ]);
    }
}
