<?php

namespace App\Http\Livewire\Profile;

use App\Models\Address;
use App\Models\Tenant;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class TenantProfile extends Component
{
    use WithFileUploads;

    public Tenant $tenant;

    public $logo;

    protected function rules()
    {
        return [
            'tenant.name' => 'required|string',
            'tenant.abn' => 'required|string',
            'logo' => 'nullable|image|max:1024',
        ];
    }

    public function updatedLogo()
    {
        $this->validate([
            'logo' => 'image|max:1024',
        ]);
    }

    public function save()
    {
        $this->validate();

        $this->tenant->save();

        $this->logo && $this->tenant->update([
            'logo' => $this->logo->store('/', 'logos'),
        ]);

        $this->notify('Profile Updated');
    }

    public function render()
    {
        return view('livewire.profile.tenant-profile');
    }
}
