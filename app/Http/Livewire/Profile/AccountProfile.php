<?php

namespace App\Http\Livewire\Profile;

use App\Models\Address;
use App\Models\BankAccount;
use App\Models\Tenant;
use Illuminate\Validation\Rule;
use Livewire\Component;

class AccountProfile extends Component
{
    public Tenant $tenant;

    public ?BankAccount $bankAccount = null;

    protected function rules()
    {
        return [
            'bankAccount.account_name' => 'required|string',
            'bankAccount.bsb' => 'required|string',
            'bankAccount.account_number' => 'required|string',
        ];
    }

    public function mount()
    {
        $this->bankAccount = $this->tenant->bankAccount ?: BankAccount::make();
    }

    public function save()
    {
        $this->validate();

        $this->bankAccount->tenant()->associate($this->tenant);
        $this->bankAccount->save();

        $this->notify('Profile Updated');
    }
    public function render()
    {
        return view('livewire.profile.account-profile');
    }
}
