<?php

namespace App\Http\Livewire\Profile;

use App\Models\Address;
use App\Models\Tenant;
use Illuminate\Validation\Rule;
use Livewire\Component;

class AddressProfile extends Component
{
    public Tenant $tenant;

    public ?Address $address = null;

    protected function rules()
    {
        return [
            'address.contact_name' => 'required|string',
            'address.contact_number' => 'required|string',
            'address.address_line_1' => 'string',
            'address.address_line_2' => 'string',
            'address.city' => 'required|string',
            'address.state' => [
                'required',
                Rule::in(Address::$states)
            ],
            'address.postcode' => 'required|string',
        ];
    }

    public function mount()
    {
        $this->address = $this->tenant->address ?: Address::make(['state' => '']);
    }

    public function save()
    {
        $this->validate();

        $this->address->tenant()->associate($this->tenant);
        $this->address->save();
        $this->tenant->address_id = $this->address->id;
        $this->tenant->save();

        $this->notify('Profile Updated');
    }

    public function render()
    {
        return view('livewire.profile.address-profile');
    }
}
