<?php

namespace App\Http\Livewire;

use App\Models\Tenant;
use Livewire\Component;
use Livewire\WithFileUploads;

class Profile extends Component
{
    use WithFileUploads;

    public Tenant $tenant;

    public int $tenantId;

    public string $currentTab = 'general';

    public function mount()
    {
        $user = auth()->user();
        $this->tenant = $user->isTenant() ? $user->tenants()->first() : Tenant::first();
        $this->tenantId = $this->tenant->id;
    }

    public function updatingTenantId($value)
    {
        $this->tenant = Tenant::find((int) $value);
        $this->resetValidation();
    }

    public function setPrimary()
    {
        $primaryTenant = auth()->user()->primaryTenant();

        if ($primaryTenant && $this->tenant->id !== $primaryTenant->id) {
            $primaryTenant->unsetPrimary();
        }

        $this->tenant->setPrimary();

        $this->notify('Set primary company successfully');
    }


    public function render()
    {
        return view('livewire.profile');
    }
}
