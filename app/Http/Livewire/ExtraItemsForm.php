<?php

namespace App\Http\Livewire;

use App\Models\CustomerQuote;
use App\Models\ExtraItem;
use App\Models\Quote;
use Livewire\Component;

class ExtraItemsForm extends Component
{
    public array $extraItems = [];

    public string $description = '';

    public ?int $quantity = null;

    public ?float $amount = null;

    public ?CustomerQuote $customerQuote = null;

    protected $listeners = ['saveItems' => 'saveItems'];

    protected array $rules = [
          'description' => 'required|max:100',
          'quantity' => 'required|integer',
          'amount' => 'required|numeric'
    ];

    public function mount()
    {
        $this->extraItems = $this->customerQuote->extraItems->all();
    }

    public function addItem()
    {
        $this->validate();

        $this->extraItems[] = [
            'description' => $this->description,
            'quantity' => $this->quantity,
            'amount' => $this->amount,
        ];
        $this->reset('description', 'quantity', 'amount');
    }

    public function removeItem($index)
    {
        array_splice($this->extraItems, $index, 1);
    }

    public function saveItems()
    {
        $extraItems = [];
        foreach ($this->extraItems as $item) {
            $extraItems[] = ExtraItem::make($item);
        }

        $this->customerQuote->extraItems()->delete();
        $this->customerQuote->extraItems()->saveMany($extraItems);

        $this->customerQuote->save();

        $this->extraItems = $this->customerQuote->extraItems->all();
    }

    public function render()
    {
        return view('livewire.extra-items-form');
    }
}
