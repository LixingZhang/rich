<?php

namespace App\Http\Livewire;

use App\Http\Livewire\DataTable\WithBulkActions;
use App\Http\Livewire\DataTable\WithCachedRows;
use App\Http\Livewire\DataTable\WithPerPagePagination;
use App\Http\Livewire\DataTable\WithSorting;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Tenants extends Component
{
    use WithPerPagePagination, WithSorting, WithBulkActions, WithCachedRows;

    public bool $showEditModal = false;
    public bool $showDeleteModal = false;

    public Tenant $editing;

    public array $filters = [
        'search'     => '',
    ];

    protected $rules = [
        'editing.name' => 'required',
        'editing.abn' => 'required',
    ];

    public function create()
    {
        Gate::authorize('create', Tenant::class);

        $this->useCachedRows();

        $this->editing = Tenant::make();

        $this->showEditModal = true;

        $this->resetValidation();
    }

    public function save()
    {
        Gate::authorize('create', Tenant::class);

        $this->validate();

        $this->editing->save();

        $this->showEditModal = false;
    }


    public function getRowsQueryProperty()
    {
        $query = Tenant::query()->withCount('users')
//            ->select('users.*')
//            ->join('tenants', 'tenants.id', '=', 'users.tenant_id')
            ->when($this->filters['search'], fn($query, $search) => $query->where('name', 'like', '%' . $search . '%')
                ->orWhere('abn', 'like', '%' . $search . '%'));

        return $this->applySorting($query);
    }

    public function getRowsProperty()
    {
        return $this->cache(function () {
            return $this->applyPagination($this->rowsQuery);
        });
    }

    public function render()
    {
        return view('livewire.tenants', [
            'tenants' => $this->rows
        ]);
    }
}
