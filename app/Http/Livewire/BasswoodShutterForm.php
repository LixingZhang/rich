<?php

namespace App\Http\Livewire;

use App\Models\BasswoodShutter;
use App\Models\RollerBlind;
use Illuminate\Validation\Rule;
use Livewire\Component;

class BasswoodShutterForm extends Component
{
    public BasswoodShutter $editing;

    protected $listeners = [
        'addProduct' => 'addProduct',
        'setProductData' => 'setProductData'
    ];

    public array $rules = [
        'name'         => 'required|max:10',
        'width'        => 'required|numeric|gt:300',
        'drop'         => 'required|numeric|gt:300',
        'shutter_type' => 'required'
    ];

    public function rules()
    {
        return [
            'editing.name' => 'required|max:255',
            'editing.shutter_type' => [
                'required',
                Rule::in(
                    'Standard',
                    'Sliding',
                    'Bifold Track',
                    'U Channel'
                )
            ],
            'editing.corner' => [
                'required',
                Rule::in('No', '90', '135')
            ],
            'editing.panel_layout' => [
                'required',
                'regex:/^[C|D|L|R|T|-]+$/i'
            ],
            'editing.in_or_out' => [
                'required',
                Rule::in('In', 'Out')
            ],
            'editing.width'           => 'required|integer|min:300',
            'editing.drop'            => 'required|integer|min:300',
            'editing.mid_rail'        => [
                'required',
                Rule::in(
                    'NA',
                    'Centre',
                    '1',
                    '2',
                    '3'
                )
            ],
            'editing.mid_rail_height' => [

            ],
            'editing.blade_size'      => [
                'required',
                Rule::in(
                    '47mm',
                    '89mm',
                    '64mm',
                    '114mm'
                )
            ],
            'editing.tilt_rod'        => [
                'required',
                Rule::in(
                    'Clear View',
                    'Central'
                )
            ],
            'editing.colour'          => [
                'required',
                Rule::in(
                    'W100 Snow',
                    'W101 Dove',
                    'W104 China',
                    'W105 Soft Pearl',
                    'W107 Pearl',
                    'W109 Linen Wash',
                    'W115 Clay',
                    'W118 Wheat',
                    'W401 Eggshell',
                    'W402 Palm Beach',
                    'W00 Custom',
                    'S430 Black Ash',
                    'S508 Jet',
                    'S602 Maple',
                    'S604 Aged Teak',
                    'S606 Mahogan',
                    'S701 Walnut'
                )
            ],
            'editing.stile_type'      => [
                'required',
                Rule::in(
                    '50b',
                    '60b',
                    '50f',
                    '60f'
                )
            ],
            'editing.frame'           => [
                'required',
                Rule::in(
                    'No Frame',
                    'FL50-B9',
                    'BL50-B10',
                    'BC65-B14A',
                    'BC50-B14',
                    'Z35-C7',
                    'Z20-C6',
                    'CZ25-D1',
                    'CZ25-D2',
                    'BL65-B10A'
                )
            ],
            'editing.frame_options'   => [
                'required',
                Rule::in('NA', 'LRTB', 'LRT', 'LRB')
            ],
            'editing.hinge_type'      => [
                'required',
                Rule::in('NA', 'Non Mortised', 'Pivot', 'Hang strip')
            ],
            'editing.hinge_colour'    => [
                'required',
                Rule::in(
                    'White',
                    'Bright Brass',
                    'Nickel',
                    'Black',
                    'Stainless',
                    'Colour Match'
                )
            ],
            'editing.notes'            => [

            ]
        ];
    }
//    public function updatedWidth()
//    {
//        $this->srq = $this->width * $this->drop;
//    }

    public function mount()
    {
        $this->editing = $this->makeBlankBasswoodShutter();
    }

    private function makeBlankBasswoodShutter()
    {
        return BasswoodShutter::make([
            'shutter_type'    => '',
            'corner'          => '',
            'panel_layout'    => '',
            'in_or_out'       => '',
            'mid_rail'        => '',
            'mid_rail_height' => '',
            'blade_size'      => '',
            'tilt_rod'        => '',
            'stile_type'      => '',
            'colour'          => '',
            'frame'           => '',
            'frame_options'   => '',
            'hinge_type'      => '',
            'hinge_colour'    => '',
            'notes'           => ''
        ]);
    }

    public function addProduct()
    {
        $this->validate();

        $this->emitUp('productAdded', 'basswood_shutters', $this->editing);
    }

    public function setProductData($product = null)
    {
        if ($product) {
            $this->editing = BasswoodShutter::make($product);
        }

        $this->resetErrorBag();
    }


    public function render()
    {
        return view('livewire.basswood-shutter-form');
    }
}
