<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use Illuminate\Http\Request;

class QuoteController extends Controller
{
    public function __invoke(Quote $quote)
    {
        $pdf = \PDF::loadView('pdf.quote', compact('quote'));

        return $pdf->stream(sprintf('%s.pdf', $quote->po_reference));
    }
}
