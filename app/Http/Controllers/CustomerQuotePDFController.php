<?php

namespace App\Http\Controllers;

use App\Models\CustomerQuote;
use Illuminate\Http\Request;

class CustomerQuotePDFController extends Controller
{
    public function __invoke(CustomerQuote $customerQuote, Request $request)
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        $pdf = \PDF::loadView('pdf.customer-quote', compact('customerQuote'));

        return $pdf->stream(sprintf('%s.pdf', $customerQuote->po_reference));
    }
}
