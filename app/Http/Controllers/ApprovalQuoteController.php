<?php

namespace App\Http\Controllers;

use App\Models\CustomerQuote;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ApprovalQuoteController extends Controller
{
    public function store(CustomerQuote $customerQuote, Request $request): RedirectResponse
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        $customerQuote->approve();

        return redirect()->back();
    }
}
