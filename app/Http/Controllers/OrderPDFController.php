<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Quote;

class OrderPDFController extends Controller
{
    public function __invoke(Order $order)
    {
//        return view('pdf.order', compact('order'));
        $pdf = \PDF::loadView('pdf.order', compact('order'));

        return $pdf->stream(sprintf('%s.pdf', $order->po_reference));
    }
}
