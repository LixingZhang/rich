<?php

namespace App\Http\Controllers;

use App\Models\CustomerQuote;
use Illuminate\Http\Request;

class CustomerQuoteController extends Controller
{
    public function show(CustomerQuote $customerQuote, Request $request)
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        return view('customer-quote.show', [
            'customerQuote' => $customerQuote
        ]);
    }



}
