<?php

namespace App\Mail;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuoteEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Quote
     */
    private Quote $quote;
    private string $content;

    /**
     * Create a new message instance.
     *
     * @param Quote  $quote
     * @param string $subject
     * @param string $content
     */
    public function __construct(Quote $quote, string $subject = '', string $content = '')
    {
        //
        $this->quote = $quote;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf = \PDF::loadView('pdf.quote', ['quote' => $this->quote]);

        return $this
            ->subject($this->subject)
            ->text($this->content)
            ->attachData($pdf->output(), 'name.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
