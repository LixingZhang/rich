<x-layouts.base>
    <div class="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
{{--        <div class="sm:mx-auto sm:w-full sm:max-w-md">--}}
{{--            <img class="mx-auto h-10 w-auto" src="/img/logo-dark.svg" alt="Surge Logo" />--}}
{{--        </div>--}}

        <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
            <div class="py-8 px-4 sm:rounded-lg sm:px-10">
                {{ $slot }}
            </div>
        </div>
    </div>
</x-layouts.base>
