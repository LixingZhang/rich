<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css" media="screen">
        html {
            font-family: sans-serif;
            /*line-height: 1.15;*/
            margin: 0;
        }
        body {
            /*font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";*/
            font-weight: 400;
            color: #212529;
            text-align: left;
            background-color: #fff;
            margin: 36pt;
        }
        h4 {
            margin-top: 0;
            margin-bottom: 0.5rem;
        }
        p {
            margin:0;
        }
        img {
            vertical-align: middle;
            border-style: none;
        }
        table {
            border-collapse: collapse;
        }
        th {
            text-align: inherit;
        }
        h4, .h4 {
            margin-bottom: 0.5rem;
            font-weight: 500;
            line-height: 1.2;
        }
        h4, .h4 {
            font-size: 1.5rem;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
        }
        .table th,
        .table td {
            padding-top: 0.75rem;
            padding-bottom: 0.75rem;
            vertical-align: top;
            /*border-top: 1px solid #dee2e6;*/
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table tbody + tbody {
            border-top: 2px solid #dee2e6;
        }
        .mt-1 {
            margin-top: 0.75rem !important;
        }
        .mt-5 {
            margin-top: 3rem !important;
        }
        .text-sm {
            font-size: 0.875rem;
            line-height: 1.25rem;
        }
        .text-xs {
            font-size: 0.75rem;
            line-height: 1rem;
        }
        .text-2xl {
            font-size: 1.5rem;
            line-height: 2rem;
        }
        .pr-0,
        .px-0 {
            padding-right: 0 !important;
        }
        .pl-0,
        .px-0 {
            padding-left: 0 !important;
        }
        .py-1 {
            padding-top: 0.5rem !important;
            padding-bottom: 0.5rem !important;
        }
        .border-b-2 {
            border-bottom:2px solid black
        }
        .pl-10 {
            padding-left: 3rem !important;
        }
        .text-right {
            text-align: right !important;
        }
        .text-center {
            text-align: center !important;
        }
        .text-uppercase {
            text-transform: uppercase !important;
        }

    </style>
</head>
<body>
    <div>
        @yield('content')
    </div>
</body>
</html>
