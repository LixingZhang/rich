@extends('layouts.pdf')

@section('content')
    <table class="table mt-5">
        <tbody>
        <tr>
            <td>
                <h1 class="text-2xl">QUOTE</h1>
                <div class="text-xs pl-10 mt-5">
                    <p>{{ $customerQuote->customer->name }}</p>
                    <p>{{ $customerQuote->customer->address }}</p>
                    <p>{{ $customerQuote->customer->city }} {{ $customerQuote->customer->state }} {{ $customerQuote->customer->postcode }}</p>
                </div>
            </td>
            <td class="space-y-1">
                <div>
                    <p class="text-sm">Date</p>
                    <p class="text-xs">{{ $customerQuote->created_at->format('d M Y') }}</p>
                </div>
                <div class="mt-1">
                    <p class="text-sm">Expiry</p>
                    <p class="text-xs">26 Dec 2020</p>
                </div>
                <div class="mt-1">
                    <p class="text-sm">Quote Number</p>
                    <p class="text-xs">CQU-{{ $customerQuote->id }}</p>
                </div>
                <div class="mt-1">
                    <p class="text-sm">Reference</p>
                    <p class="text-xs">{{ $customerQuote->reference }}</p>
                </div>
                <div class="mt-1">
                    <p class="text-sm">ABN</p>
                    <p class="text-xs">{{ $customerQuote->tenant->abn }}</p>
                </div>
            </td>
            <td>
                <div class="text-sm">
                    <p>{{ $customerQuote->tenant->name }}</p>
                    <p>{{ $customerQuote->tenant->address_line_1 }}</p>
                    <p>{{ $customerQuote->tenant->city }}</p>
                    <p>{{ $customerQuote->tenant->state }} {{ $customerQuote->tenant->postcode }}</p>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="table mt-40 w-full">
        <thead>
        <tr class="text-sm border-b-2">
            <td class="py-2 border-b-2" style="width: 33%">Description</td>
            <td class="text-right py-1 border-b-2">Quantity</td>
            <td class="text-right py-1 border-b-2">Unit Price</td>
            <td class="text-right py-1 border-b-2">Discount</td>
            <td class="text-right py-1 border-b-2">GST</td>
            <td class="text-right py-1 border-b-2">Amount AUD</td>
        </tr>
        </thead>
        <tbody>
        @php
            $hasDiscount = false;
        @endphp
        @foreach($customerQuote->items as $item)
            <tr class="text-xs">
                <td class="py-1">{{ $item->productable->name }}
                </td>
                <td class="text-right">1</td>
                <td class="text-right">
                    $ {{ $item->unit_price }}
                </td>
                <td class="text-right">{{ $item->discount ? $item->discount->discount . '%' : '' }}</td>
                <td class="text-right">10%</td>
                <td class="text-right">$ {{ $item->amount }}</td>
            </tr>
        @endforeach

        <tr>
            @if($hasDiscount)
                <td class="text-xs py-1 border-b-2" colspan="6">10% DISCOUNT APPLIED TO ALL SHUTTERS</td>
            @else
                <td class="text-xs py-0 border-b-2" colspan="6"></td>
            @endif
        </tr>

        <tr>
            <td colspan="1">
            </td>
            <td class="text-right text-sm py-1 border-b-2" colspan="4">
                <p>Subtotal (includes a discount of 1,324.12)</p>
                <p>TOTAL GST 10%</p>
            </td>
            <td class="text-right text-sm py-1 border-b-2">
                <p>$ {{ $customerQuote->amount }}</p>
                <p>$ {{ round($customerQuote->amount * 0.1, 2) }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td class="text-right text-sm py-1 border-b-2" colspan="4">
                TOTAL AUD
            </td>
            <td class="text-right text-sm py-1 border-b-2" >
                $ {{ round($customerQuote->amount * 1.1, 2) }}
            </td>
        </tr>
        </tbody>
    </table>

    <div class="mt-5">
        <p class="text-sm border-gray-800 py-1 border-b-2">Terms</p>
        <div class="text-xs mt-1">
            <p>{{ $customerQuote->tenant->terms }}</p>
        </div>
        <div class="text-xs mt-5">
            <p>Account Name - {{ $customerQuote->tenant->bankAccount->account_name }}</p>
            <p>BSB - {{ $customerQuote->tenant->bankAccount->bsb }}</p>
            <p>ACC - {{ $customerQuote->tenant->bankAccount->account_number }}</p>
            <p>Please use quote number as description</p>
        </div>
    </div>
@endsection
