<x-layouts.base>
    <div class="container mx-auto px-40">
        <div class="mt-10 flex justify-between">
            @if (!$customerQuote->isApproved())
            <a href="{{ URL::signedRoute('customer-quotes.approve', ['customerQuote' => $customerQuote->id])}}">
            <x-button.primary>Approve</x-button.primary>
            </a>
            @endif
            <a href="{{ URL::signedRoute('customer-quotes.pdf', ['customerQuote' => $customerQuote->id])}}">
                <x-button.primary>Print</x-button.primary>
            </a>
        </div>

        <div class="mt-20 grid grid-cols-4">
                <div class="col-span-2">
                    <h1 class="text-2xl">QUOTE</h1>
                    <div class="text-xs pl-10 mt-5">
                        <p>{{ $customerQuote->customer->name }}</p>
                        <p>{{ $customerQuote->customer->address }}</p>
                        <p>{{ $customerQuote->customer->city }} {{ $customerQuote->customer->state }} {{ $customerQuote->customer->postcode }}</p>
                    </div>
                </div>
                <div class="space-y-1">
                    <div>
                        <p class="text-sm">Date</p>
                        <p class="text-xs">{{ $customerQuote->created_at->format('d M Y') }}</p>
                    </div>
                    <div class="mt-1">
                        <p class="text-sm">Expiry</p>
                        <p class="text-xs">26 Dec 2020</p>
                    </div>
                    <div class="mt-1">
                        <p class="text-sm">Quote Number</p>
                        <p class="text-xs">CQU-{{ $customerQuote->id }}</p>
                    </div>
                    <div class="mt-1">
                        <p class="text-sm">Reference</p>
                        <p class="text-xs">{{ $customerQuote->reference }}</p>
                    </div>
                    <div class="mt-1">
                        <p class="text-sm">ABN</p>
                        <p class="text-xs">{{ $customerQuote->tenant->abn }}</p>
                    </div>
                </div>
                <div>
                    <div class="text-sm">
                        <p>{{ $customerQuote->tenant->name }}</p>
                        <p>{{ $customerQuote->tenant->address_line_1 }}</p>
                        <p>{{ $customerQuote->tenant->city }}</p>
                        <p>{{ $customerQuote->tenant->state }} {{ $customerQuote->tenant->postcode }}</p>
                    </div>
                </div>
        </div>

        <table class="table mt-28 w-full">
            <thead>
            <tr class="text-sm">
                <td class="py-2 border-b-2 border-gray-800" style="width: 33%">Description</td>
                <td class="text-right py-1 border-b-2 border-gray-800">Quantity</td>
                <td class="text-right py-1 border-b-2 border-gray-800">Unit Price</td>
                <td class="text-right py-1 border-b-2 border-gray-800">Discount</td>
                <td class="text-right py-1 border-b-2 border-gray-800">GST</td>
                <td class="text-right py-1 border-b-2 border-gray-800">Amount AUD</td>
            </tr>
            </thead>
            <tbody>
            @php
                $hasDiscount = false;
            @endphp
            @foreach($customerQuote->items as $item)
                <tr class="text-xs">
                    <td class="py-1">{{ $item->productable->name }}
                    </td>
                    <td class="text-right">1</td>
                    <td class="text-right">
                        $ {{ $item->unit_price }}
                    </td>
                    <td class="text-right">{{ $item->discount ? $item->discount->discount . '%' : '' }}</td>
                    <td class="text-right">10%</td>
                    <td class="text-right">$ {{ $item->amount }}</td>
                </tr>
            @endforeach

            <tr>
                @if($hasDiscount)
                    <td class="text-xs py-1 border-b-2 border-gray-800" colspan="6">10% DISCOUNT APPLIED TO ALL SHUTTERS</td>
                @else
                    <td class="text-xs py-0 border-b-2 border-gray-800" colspan="6"></td>
                @endif
            </tr>

            <tr>
                <td colspan="1">
                </td>
                <td class="text-right text-sm py-1 border-b-2 border-gray-800" colspan="4">
                    <p>Subtotal (includes a discount of 1,324.12)</p>
                    <p>TOTAL GST 10%</p>
                </td>
                <td class="text-right text-sm py-1 border-b-2 border-gray-800">
                    <p>$ {{ $customerQuote->amount }}</p>
                    <p>$ {{ round($customerQuote->amount * 0.1, 2) }}</p>
                </td>
            </tr>
            <tr>
                <td colspan="1"></td>
                <td class="text-right text-sm py-1 border-b-2 border-gray-800" colspan="4">
                    TOTAL AUD
                </td>
                <td class="text-right text-sm py-1 border-b-2 border-gray-800" >
                    $ {{ round($customerQuote->amount * 1.1, 2) }}
                </td>
            </tr>
            </tbody>
        </table>

        <div class="mt-5">
            <p class="text-sm border-gray-800 py-1 border-b-2">Terms</p>
            <div class="text-xs mt-1">
                <p>{{ $customerQuote->tenant->terms }}</p>
            </div>
            <div class="text-xs mt-5">
                <p>Account Name - {{ $customerQuote->tenant->bankAccount->account_name }}</p>
                <p>BSB - {{ $customerQuote->tenant->bankAccount->bsb }}</p>
                <p>ACC - {{ $customerQuote->tenant->bankAccount->account_number }}</p>
                <p>Please use quote number as description</p>
            </div>
        </div>

    </div>
</x-layouts.base>
