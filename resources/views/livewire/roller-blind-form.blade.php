<div>
    <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
        <div class="sm:col-span-2">
            <x-input.group inline for="name" label="Name" :error="$errors->first('editing.name')">
                <x-input.text wire:model.defer="editing.name" id="name" placeholder="Name"></x-input.text>
            </x-input.group>
        </div>
        <div class="sm:col-span-2">
            <x-input.group inline for="width" label="Width" :error="$errors->first('editing.width')">
                <x-input.text wire:model.defer="editing.width" id="width" placeholder="Width" trailingAddOn="mm"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="Drop" label="Drop" :error="$errors->first('editing.drop')">
                <x-input.text wire:model.defer="editing.drop" id="drop" placeholder="Drop" trailingAddOn="mm"></x-input.text>
            </x-input.group>
        </div>


        <div class="sm:col-span-2">
            <x-input.group inline for="type" label="Type" :error="$errors->first('editing.type')">
                <x-input.select wire:model="editing.type" id="type" placeholder="Select...">
                    @foreach(\App\Models\RollerBlind::$types as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="fabric" label="Fabric" :error="$errors->first('editing.fabric')">
                <x-input.select wire:model="editing.fabric" id="fabric">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$fabrics[$editing->type] ?? [] as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="colour" label="Colour" :error="$errors->first('editing.colour')">
                <x-input.select wire:model.defer="editing.colour" id="colour">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$colours[$editing->type][$editing->fabric] ?? [] as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="control_type" label="Control Type" :error="$errors->first('editing.control_type')">
                <x-input.select wire:model.defer="editing.control_type" id="control_type">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$controlTypes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="control_side" label="Control Side" :error="$errors->first('editing.control_side')">
                <x-input.select wire:model.defer="editing.control_side" id="control_side">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$controlSides as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="chain_length" label="Chain Length" :error="$errors->first('editing.chain_length')">
                <x-input.text wire:model.defer="editing.chain_length" id="chain_length" placeholder="Chain Length"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="component_colour" label="Component Colour" :error="$errors->first('editing.component_colour')">
                <x-input.select wire:model.defer="editing.component_colour" id="component_colour">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$componentColours as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="base_rail" label="Base Rail" :error="$errors->first('editing.base_rail')">
                <x-input.select wire:model.defer="editing.base_rail" id="base_rail">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$baseRails as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="roll_direction" label="Roll Direction" :error="$errors->first('editing.roll_direction')">
                <x-input.select wire:model.defer="editing.roll_direction" id="roll_direction">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$rollDirections as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="motor_type" label="Motor Type" :error="$errors->first('editing.motor_type')">
                <x-input.select wire:model.defer="editing.motor_type" id="motor_type">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$motorTypes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="charger" label="Charger" :error="$errors->first('editing.charger')">
                <x-input.select wire:model.defer="editing.charger" id="charger">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$chargers as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="wifi_hub" label="Wifi Hub" :error="$errors->first('editing.wifi_hub')">
                <x-input.select wire:model.defer="editing.wifi_hub" id="wifi_hub">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$wifiHubs as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="remote" label="Remote" :error="$errors->first('editing.remote')">
                <x-input.select wire:model.defer="editing.remote" id="remote">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$remotes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="bracket_options" label="Bracket Options" :error="$errors->first('editing.bracket_options')">
                <x-input.select wire:model.defer="editing.bracket_options" id="bracket_options">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\RollerBlind::$bracketOptions as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-6">
            <x-input.group inline for="notes" label="Notes" :error="$errors->first('editing.notes')">
                <x-input.textarea wire:model.defer="editing.notes" placeholder="Notes"></x-input.textarea>
            </x-input.group>
        </div>
    </div>
</div>
