<div class="mt-2 grid grid-cols-1 gap-y-6 gap-x-4">
{{--    <x-input.group inline for="email" label="Email" :error="$errors->first('email')">--}}
{{--        <x-input.text wire:model.defer="email" id="email" placeholder="Email"></x-input.text>--}}
{{--    </x-input.group>--}}

    <x-input.group inline for="subject" label="Subject" :error="$errors->first('subject')">
        <x-input.text wire:model.defer="subject" id="subject" placeholder="Subject"></x-input.text>
    </x-input.group>

    <x-input.group inline for="content" label="Content" :error="$errors->first('content')">
        <x-input.textarea wire:model.defer="content" id="content" placeholder="Content" rows="18"></x-input.textarea>
    </x-input.group>
</div>
