<div>
    <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
        <div class="sm:col-span-2">
            <x-input.group inline for="name" label="Name" :error="$errors->first('editing.name')">
                <x-input.text wire:model.defer="editing.name" id="name" placeholder="Name"></x-input.text>
            </x-input.group>
        </div>


        <div class="sm:col-span-2">
            <x-input.group inline for="width" label="Width" :error="$errors->first('editing.width')">
                <x-input.text wire:model.defer="editing.width" id="width" placeholder="Width" trailingAddOn="mm"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="Drop" label="Drop" :error="$errors->first('editing.drop')">
                <x-input.text wire:model.defer="editing.drop" id="drop" placeholder="Drop" trailingAddOn="mm"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="shutter_type" label="Shutter Type" :error="$errors->first('editing.shutter_type')">
                <x-input.select wire:model.defer="editing.shutter_type" id="shutter_type" placeholder="Select...">
                    @foreach(\App\Models\Shutter::$shutterTypes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="colour" label="Colour" :error="$errors->first('editing.colour')">
                <x-input.select wire:model.defer="editing.colour" id="colour">
                    <option value="" disabled>Select...</option>
                    @foreach(\App\Models\AuPvcShutter::$colours as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="corner" label="Corner" :error="$errors->first('editing.corner')">
                <x-input.select wire:model.defer="editing.corner" id="corner" placeholder="Select...">
                    @foreach(\App\Models\Shutter::$corners as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="panel_layout" label="Panel Layout" :error="$errors->first('editing.panel_layout')">
                <x-input.text wire:model.defer="editing.panel_layout" id="panel_layout" placeholder="Panel Layout"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="in_or_out" label="In or Out" :error="$errors->first('editing.in_or_out')">
                <x-input.select wire:model.defer="editing.in_or_out" id="in_or_out" placeholder="Select...">
                    @foreach(\App\Models\Shutter::$inOrOut as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="mid_rail" label="Mid Rail" :error="$errors->first('editing.mid_rail')">
                <x-input.select wire:model.defer="editing.mid_rail" id="mid_rail" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::midRails($editing->drop) as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="mid_rail_height" label="Mid Rail Height" :error="$errors->first('editing.mid_rail_height')">
                <x-input.text wire:model.defer="editing.mid_rail_height" id="mid_rail_height" placeholder="Mid Rail Height"></x-input.text>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="blade_size" label="Blade Size" :error="$errors->first('editing.blade_size')">
                <x-input.select wire:model.defer="editing.blade_size" id="blade_size" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$bladeSizes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="tilt_rod" label="Tilt Rod" :error="$errors->first('editing.tilt_rod')">
                <x-input.select wire:model.defer="editing.tilt_rod" id="tilt_rod" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$tiltRods as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="stile_type" label="Stile Type" :error="$errors->first('editing.stile_type')">
                <x-input.select wire:model.defer="editing.stile_type" id="stile_type" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$stileTypes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="frame" label="Frame" :error="$errors->first('editing.frame')">
                <x-input.select wire:model.defer="editing.frame" id="frame" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$frames as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="frame_options" label="Frame Options" :error="$errors->first('editing.frame_options')">
                <x-input.select wire:model.defer="editing.frame_options" id="frame_options" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$frameOptions as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="hinge_type" label="Hinge Type" :error="$errors->first('editing.hinge_type')">
                <x-input.select wire:model.defer="editing.hinge_type" id="hinge_type" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$hingeTypes as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-2">
            <x-input.group inline for="hinge_colour" label="Hinge Colour" :error="$errors->first('editing.hinge_colour')">
                <x-input.select wire:model.defer="editing.hinge_colour" id="hinge_colour" placeholder="Select...">
                    @foreach(\App\Models\AuPvcShutter::$hingeColours as $label)
                    <option value="{{$label}}">{{ $label }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>

        <div class="sm:col-span-6">
            <x-input.group inline for="notes" label="Notes" :error="$errors->first('editing.notes')">
                <x-input.textarea wire:model.defer="editing.notes" placeholder="Notes"></x-input.textarea>
            </x-input.group>
        </div>
    </div>
</div>