<div x-data="{ show : @entangle('addNewCustomer') }" x-cloak>
    <h1 class="text-2xl font-semibold text-gray-900">Quotes</h1>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="w-1/4">
                <x-input.text wire:model="filters.search" placeholder="Search Company / Reference..." />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <x-button.primary wire:click="submitOrder" :disabled="count($selected) == 0">
                    <x-icon.plus document class="mr-2 " /> Submit Order
                </x-button.primary>

                <x-button.primary wire:click="emailForm" :disabled="count($selected) != 1">
                    <x-icon.email class="mr-2" /> Send Email
                </x-button.primary>
            </div>
        </div>

        <!-- Quotes Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        @if (count($quotes))
                            <x-input.checkbox wire:model="selectPage" />
                        @endif
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('tenants.name')" :direction="$sorts['tenants.name'] ?? null">Company Name</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('users.name')" :direction="$sorts['users.name'] ?? null">Staff</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('reference')" :direction="$sorts['reference'] ?? null">Reference</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('amount')" :direction="$sorts['amount'] ?? null">Amount</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('created_at')" :direction="$sorts['created_at'] ?? null">Date</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                    <x-table.row class="bg-gray-200" wire:key="row-message">
                        <x-table.cell colspan="7">
                            @unless ($selectAll)
                            <div>
                                <span>You have selected <strong>{{ $quotes->count() }}</strong> transactions, do you want to select all <strong>{{ $quotes->total() }}</strong>?</span>
                                <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All</x-button.link>
                            </div>
                            @else
                            <span>You are currently selecting all <strong>{{ $quotes->total() }}</strong> transactions.</span>
                            @endif
                        </x-table.cell>
                    </x-table.row>
                    @endif

                    @forelse ($quotes as $qt)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $qt->id }}">
                        <x-table.cell class="pr-0">
                            <x-input.checkbox wire:model="selected" value="{{ $qt->id }}" />
                        </x-table.cell>

                        <x-table.cell>
                            <span class="inline-flex space-x-2 truncate text-sm leading-5">

                                <span class="text-gray-600 truncate font-medium">
                                    {{ $qt->tenant->name }}
                                </span>
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $qt->staff->name }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $qt->reference }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">${{ $qt->amount }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium"> {{ $qt->date_for_humans }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <x-button.link wire:click="edit({{ $qt->id }})">
                                <svg class="text-green-700 inline-block w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                </svg>
                            </x-button.link>

                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="7">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                <span class="font-medium py-8 text-gray-400 text-xl">No Quotes found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $quotes->links() }}
            </div>
        </div>
    </div>

    <form wire:submit.prevent="createCustomerQuote">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">Create Customer Quote</x-slot>
            <x-slot name="content">
                @if($addNewCustomer)
                    <div class="shadow sm:rounded-md sm:overflow-hidden">
                        <div class="bg-white py-4">
                            <div>
                                <h4 id="payment_details_heading" class="text-md leading-6 font-medium text-gray-600">
                                    New Customer</h4>
                            </div>
                            <div class="mt-2 grid grid-cols-4 gap-6">
                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="tenant" label="Company" :error="$errors->first('customer.tenant_id')">
                                        <x-input.select wire:model.defer="customer.tenant_id" id="tenant" placeholder="Please Select..">
                                            @foreach(auth()->user()->isTenant() ? auth()->user()->tenants : \App\Models\Tenant::all() as $tenant)
                                                <option value="{{ $tenant->id }}">{{ $tenant->name }}</option>
                                            @endforeach
                                        </x-input.select>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="name" label="Name" :error="$errors->first('customer.name')">
                                        <x-input.text wire:model.defer="customer.name" id="name"></x-input.text>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="email" label="Email" :error="$errors->first('customer.email')">
                                        <x-input.text wire:model.defer="customer.email" id="email"></x-input.text>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="contact_name" label="Contact Name" :error="$errors->first('customer.contact_name')">
                                        <x-input.text wire:model.defer="customer.contact_name" id="contact_name"></x-input.text>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="address" label="Address" :error="$errors->first('customer.address')">
                                        <x-input.text wire:model.defer="customer.address" id="address"></x-input.text>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                    <x-input.group inline for="city" label="City" :error="$errors->first('customer.city')">
                                        <x-input.text wire:model.defer="customer.city" id="city"></x-input.text>
                                    </x-input.group>
                                </div>

                                <div class="col-span-3 sm:col-span-2">
                                <x-input.group inline for="state" label="State" :error="$errors->first('customer.state')">
                                    <x-input.select wire:model.defer="customer.state" id="state" placeholder="Please Select...">
                                        @foreach(\App\Models\Address::$states as $state)
                                            <option value="{{ $state }}">{{$state}}</option>
                                        @endforeach
                                    </x-input.select>
                                </x-input.group>
                                </div>
                                <div class="col-span-3 sm:col-span-2">
                                <x-input.group inline for="postcode" label="Postcode" :error="$errors->first('customer.postcode')">
                                    <x-input.text wire:model.defer="customer.postcode" id="postcode"></x-input.text>
                                </x-input.group>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="flex justify-between">
                        <div class="w-3/4">
                            <x-input.group inline for="customer" label="Customer" :error="$errors->first('customerQuote.customer_id')">
                                <x-input.select wire:model.lazy="customerQuote.customer_id" id="customer" placeholder="Please Select...">
                                    @foreach(auth()->user()->customers as $cm)
                                    <option value="{{$cm->id}}">{{$cm->name}}</option>
                                    @endforeach
                                </x-input.select>
                            </x-input.group>
                        </div>
                        <div class="mt-6">
                            <x-button.primary
                                wire:click="$set('addNewCustomer', true)"
                            >New Customer</x-button.primary>
                        </div>
                    </div>
                @endif
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="bg-white py-4">
                        @if($addNewCustomer)
                        <div>
                            <h4 id="payment_details_heading" class="text-md leading-6 font-medium text-gray-600">
                                Markup</h4>
                        </div>
                        @endif
                        <div class="mt-2 grid grid-cols-4 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                                <x-input.group inline for="markup_type" label="Markup Type" :error="$errors->first('customerQuote.markup_type')">
                                    <x-input.select wire:model.lazy="customerQuote.markup_type" id="discount" placeholder="Please Select...">
                                        <option value="fixed">Fixed Markup</option>
                                        <option value="percent">Percentage Markup</option>
                                    </x-input.select>
                                </x-input.group>
                            </div>

                            <div class="col-span-3 sm:col-span-2">
                                <x-input.group inline for="markup_value" label="Markup Value" :error="$errors->first('customerQuote.markup_value')">
                                    <x-input.text wire:model.defer="customerQuote.markup_value" id="markup_value" placeholder="Markup Value"
                                                  trailingAddOn="{{ $customerQuote && $customerQuote->markup_type == \App\Models\CustomerQuote::MARKUP_PERCENT ? '%' : 'AUD' }}"
                                    ></x-input.text>
                                </x-input.group>
                            </div>

                        </div>
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="cancel">Cancel</x-button.secondary>
                <x-button.primary type="submit">Create</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    @if($showEmailModal)
        <form wire:submit.prevent="sendEmail">
            <x-modal.dialog wire:model.defer="showEmailModal">
                <x-slot name="title">Send Email</x-slot>
                <x-slot name="content">
                    @livewire('email-form', ['subject' => $subject, 'content' => $content])
                </x-slot>
                <x-slot name="footer">
                    <x-button.secondary wire:click="$set('showEmailModal', false)">Cancel</x-button.secondary>
                    <x-button.primary type="submit">Send</x-button.primary>
                </x-slot>
            </x-modal.dialog>
        </form>
    @endif
</div>
