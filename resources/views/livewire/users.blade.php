<div>
    <div class="px-4 sm:px-6 md:px-0">
        <h1 class="text-3xl font-extrabold text-gray-900">Users</h1>
    </div>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="w-1/4">
                <x-input.text wire:model="filters.search" placeholder="Search User / Company..." />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

{{--                @can('delete', \App\Models\ProductDiscount::class)--}}
{{--                    <x-dropdown label="Bulk Actions">--}}
{{--                        --}}{{-- <x-dropdown.item type="button" wire:click="exportSelected" class="flex items-center space-x-2">--}}
{{--                        --}}{{-- <x-icon.download class="text-gray-400"/> <span>Export</span>--}}
{{--                        --}}{{-- </x-dropdown.item>--}}

{{--                        <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">--}}
{{--                            <x-icon.trash class="text-gray-400" />--}}
{{--                            <span>Delete</span>--}}
{{--                        </x-dropdown.item>--}}
{{--                    </x-dropdown>--}}
{{--                @endcan--}}

                @can('create', \App\Models\User::class)
                    <x-button.primary wire:click="create">
                        <x-icon.plus document class="mr-2" />
                        New
                    </x-button.primary>
                @endcan
            </div>
        </div>

        <!-- Quotes Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        @if (count($users))
                            <x-input.checkbox wire:model="selectPage" />
                        @endif
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('name')" :direction="$sorts['name'] ?? null">Name
                    </x-table.heading>
                    <x-table.heading >Company
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('role')" :direction="$sorts['role'] ?? null">Role
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('created_at')" :direction="$sorts['created_at'] ?? null">Date
                    </x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-gray-200" wire:key="row-message">
                            <x-table.cell colspan="6">
                                @unless ($selectAll)
                                    <div>
                                        <span>You have selected <strong>{{ $users->count() }}</strong> rows, do you want to select all <strong>{{ $users->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All
                                        </x-button.link>
                                    </div>
                                @else
                                    <span>You are currently selecting all <strong>{{ $users->total() }}</strong> rows.</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($users as $user)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $user->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $user->id }}" />
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium">{{ $user->name }} </span>
                            </x-table.cell>

                            <x-table.cell>
                            <span class="inline-flex space-x-2 truncate text-sm leading-5">

                                <span class="text-gray-600 truncate font-medium">
                                    @foreach($user->tenants as $tenant)
                                    <p>{{ $tenant->name }}</p>
                                    @endforeach
                                </span>
                            </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium">{{ $user->role }}</span>
                            </x-table.cell>


                            <x-table.cell>
                                <span class="text-gray-600 font-medium"> {{ $user->date_for_humans }} </span>
                            </x-table.cell>

                            <x-table.cell>
{{--                                @can('update', $user)--}}
{{--                                    <x-button.link wire:click="edit({{ $user->id }})">--}}
{{--                                        <x-icon.edit />--}}
{{--                                    </x-button.link>--}}
{{--                                @endcan--}}
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="8">
                                <div class="flex justify-center items-center space-x-2">
                                    <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                    <span class="font-medium py-8 text-gray-400 text-xl">No Users found...</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $users->links() }}
            </div>
        </div>
    </div>

{{--    <!-- Delete Product Discount Modal -->--}}
{{--    <form wire:submit.prevent="deleteSelected">--}}
{{--        <x-modal.confirmation wire:model.defer="showDeleteModal">--}}
{{--            <x-slot name="title">Delete User</x-slot>--}}

{{--            <x-slot name="content">--}}
{{--                <div class="py-8 text-gray-700">Are you sure you? This action is irreversible.</div>--}}
{{--            </x-slot>--}}

{{--            <x-slot name="footer">--}}
{{--                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>--}}

{{--                <x-button.primary type="submit">Delete</x-button.primary>--}}
{{--            </x-slot>--}}
{{--        </x-modal.confirmation>--}}
{{--    </form>--}}

    <!-- Save Product Discount Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">
                @if($editing && !$editing->id) Create @else Edit @endif User
            </x-slot>
            <x-slot name="content">
                @if ($editing && !$editing->id)
                    <x-input.group for="tenants" label="Companies" :error="$errors->first('tenantIds')">
                        <x-input.select multiple wire:model.defer="tenantIds" id="tenants" placeholder="Please Select...">
                            @foreach(auth()->user()->isTenant() ? auth()->user()->tenants : \App\Models\Tenant::all() as $tenant)
                                <option value="{{ $tenant->id }}">{{ $tenant->name }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group for="email" label="Email" :error="$errors->first('editing.email')">
                        <x-input.text wire:model.defer="editing.email" id="email" placeholder="Email"></x-input.text>
                    </x-input.group>

                    <x-input.group for="name" label="Name" :error="$errors->first('editing.name')">
                        <x-input.text wire:model.defer="editing.name" id="last_name" placeholder="Name"></x-input.text>
                    </x-input.group>

                    <x-input.group for="role" label="Role" :error="$errors->first('editing.role')">
                        <x-input.select wire:model.defer="editing.role" id="role" placeholder="Please Select...">
                            @foreach(\App\Models\User::$roles as $role)
                                <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

{{--                    <x-input.group for="first_name" label="First Name" :error="$errors->first('editing.first_name')">--}}
{{--                        <x-input.text wire:model.defer="editing.first_name" id="first_name" placeholder="First Name"></x-input.text>--}}
{{--                    </x-input.group>--}}
                @endif

{{--                <x-input.group for="discount" label="Discount" :error="$errors->first('editing.discount')">--}}
{{--                    <x-input.text wire:model.defer="editing.discount" id="discount" placeholder="Discount" trailingAddOn="% OFF"></x-input.text>--}}
{{--                </x-input.group>--}}
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

</div>
