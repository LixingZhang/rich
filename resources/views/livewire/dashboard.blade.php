<div x-data="{ tab: 'basswood_shutters' }" x-cloak>
    <h1 class="text-2xl font-semibold text-gray-900">Home</h1>
    <p class="max-w-4xl text-sm leading-5 text-gray-500">Workcation is a property rental website. Etiam ullamcorper
        massa viverra consequat, consectetur id nulla tempus. Fringilla egestas justo massa purus sagittis
        malesuada.</p>
    <form wire:submit.prevent="submit">
        <div class="mt-10 py-4 space-y-4">
            <!--
              Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
              Read the documentation to get started: https://tailwindui.com/documentation
            -->
            <div>
            <div class="pb-5 border-b border-gray-200">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Order Summery
                </h3>

            </div>
            <div class="sm:p-0">
                <dl>
                    @foreach($pricing as $name => $value)
                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:py-4">
                        <dt class="text-sm leading-5 font-medium text-gray-500">
                            {{ $name }}
                            @if ($value['discount'])
                                <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-indigo-100 text-indigo-800 uppercase">
                                          {{ $value['discount'] }}% off
                                </span>
                            @endif

                        </dt>
                        <dd class="mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2">
                            ${{ $value['discounted_price'] ?: $value['base_price'] }} AUD
                        </dd>
                    </div>
                    @endforeach


                    <div class="mt-8 sm:mt-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:border-t sm:border-gray-200 sm:py-4">
                        <dt class="text-md leading-6 font-medium text-gray-900">
                            Sum
                        </dt>
                        <dd class="mt-1 leading-6 font-medium text-gray-900 sm:mt-0 sm:col-span-2">
                            ${{$sum}} AUD
                        </dd>
                    </div>
                </dl>
            </div>
            </div>
            <div>
            <!--
              Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
              Read the documentation to get started: https://tailwindui.com/documentation
            -->
            <div class=" pb-5 border-b border-gray-200 space-y-3 sm:flex sm:items-center sm:justify-between sm:space-x-4 sm:space-y-0">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Basic Information
                </h3>
                <div class="flex space-x-3">
                    <x-button.secondary wire:click="resetForm">
                        <svg class="mr-2 w-5 h-5 text-red-600 transition ease-in-out duration-150" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                        <span class="text-red-600"> Reset Form</span>
                    </x-button.secondary>
                    <x-button.primary wire:click="submitQuote" :disabled="!$canSubmit">
                        <svg class="mr-2 h-5 w-5  transition ease-in-out duration-150" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
                        </svg>
                        Submit Quote
                    </x-button.primary>
                    <x-button.primary wire:click="submitOrder" :disabled="!$canSubmit">
                        <svg class="mr-2 h-5 w-5 transition ease-in-out duration-150" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                        </svg>
                        Submit Order
                    </x-button.primary>
                </div>
            </div>
            <div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                <div class="sm:col-span-2">
                    <x-input.group inline for="tenant" label="Company" :error="$errors->first('form.tenant')">
                        <x-input.select wire:model.debounce.1000ms="form.tenant" id="tenant" placeholder="Please Select...">
                            @foreach($user->isTenant() ? $user->tenants : \App\Models\Tenant::all() as $tenant)
                                <option value="{{ $tenant->id }}"> {{ $tenant->name }} </option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>
                </div>
                <div class="sm:col-span-2">
                    <x-input.group inline for="reference" label="Reference" :error="$errors->first('form.reference')">
                        <x-input.text wire:model.debounce.1000ms="form.reference" id="reference" placeholder="Reference"></x-input.text>
                    </x-input.group>
                </div>

            </div>
            <div class="mt-4">
                <x-input.group inline for="notes" label="Notes" :error="$errors->first('form.notes')">
                    <x-input.textarea wire:model.debounce.1000ms="form.notes" id="notes" placeholder="Notes"></x-input.textarea>
                </x-input.group>
            </div>

            </div>

            <div>
            <!--
              Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
              Read the documentation to get started: https://tailwindui.com/documentation
            -->
            <div class="pb-5 border-b border-gray-200 space-y-3 sm:flex sm:items-center sm:justify-between sm:space-x-4 sm:space-y-0">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Product Lists
                </h3>
                <div class="flex space-x-3">
                    <x-button.primary wire:click="showProductModal">
                        <x-icon.plus document class="mr-2" />
                        Add Product
                    </x-button.primary>

                </div>
            </div>

            <!--
              Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
              Read the documentation to get started: https://tailwindui.com/documentation
            -->
            <div>
                <div class="sm:hidden">
                    <select aria-label="Selected tab" class="form-select block w-full">
                        @foreach($productHeaders as $product)
                        <option>{{$product['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="hidden sm:block">
                    <div class="border-b border-gray-200">
                        <!-- Tab links -->
                        <nav class="-mb-px flex">
                            @foreach($productHeaders as $product)
                            <a href="#" @click="tab = '{{$product['key']}}'" wire:click="$set('currentProduct', '{{$product['name']}}')" :class="{'border-indigo-500 text-indigo-600 focus:outline-none focus:text-indigo-800 focus:border-indigo-700': tab == '{{$product['key']}}' }" class="w-1/4 py-4 px-1 text-center border-b-2 border-transparent font-medium text-sm leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300" aria-current="page">
                                {{ $product['name'] }}
                            </a>
                            @endforeach
                        </nav>
                    </div>

                    <!-- Tab content -->
                    @foreach($productHeaders as $product)
                    <div x-show="tab == '{{$product['key']}}'">
                        <x-table>
                            <x-slot name="head">
                                @foreach($product['headers'] as $header)
                                <x-table.heading class="w-5 py-2">
                                    {{ $header['text'] }}
                                </x-table.heading>
                                @endforeach
                            </x-slot>

                            <x-slot name="body">
                                @forelse($form[$product['key']] as $index => $item)
                                <x-table.row>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['name'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['width'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['drop'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['width'] * $item['drop'] / 1000000 }}</span></x-table.cell>

                                    @switch($product['key'])
                                    @case('roller_blinds')
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['fabric'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['colour'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['control_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['control_side'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['chain_length'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['component_colour'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['base_rail'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['roll_direction'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['motor_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['charger'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['wifi_hub'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['remote'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['bracket_options'] }} </span></x-table.cell>
                                    @break
                                    @case('basswood_shutters')
                                    @case('au_pvc_shutters')
                                    @case('pvc_shutters')
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['shutter_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['corner'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['panel_layout'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ strlen($item['panel_layout']) }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['in_or_out'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['mid_rail'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['mid_rail_height'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['blade_size'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['tilt_rod'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['colour'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['stile_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['frame'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['frame_options'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['hinge_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['hinge_colour'] }} </span></x-table.cell>
                                    @break
                                    @case('aluminium_shutters')
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['shutter_type'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['corner'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['panel_layout'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ strlen($item['panel_layout']) }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['in_or_out'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['mid_rail'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['mid_rail_height'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['blade_size'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['tilt_rod'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['colour'] }} </span></x-table.cell>
                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['frame'] }} </span></x-table.cell>
                                    @break
                                    @endswitch

                                    <x-table.cell><span class="text-gray-900 font-medium"> {{ $item['notes'] ?? '' }} </span></x-table.cell>

                                    <x-table.cell>
                                        <x-button.link wire:click="duplicate('{{$product['key']}}', {{ $index }})">
                                            <svg class="w-6 h-6 text-indigo-600" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 16H6a2 2 0 01-2-2V6a2 2 0 012-2h8a2 2 0 012 2v2m-6 12h8a2 2 0 002-2v-8a2 2 0 00-2-2h-8a2 2 0 00-2 2v8a2 2 0 002 2z"></path>
                                            </svg>
                                        </x-button.link>
                                        <x-button.link wire:click="delete('{{$product['key']}}', {{ $index }})">
                                            <svg class="w-6 h-6 text-red-600" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                            </svg>
                                        </x-button.link>
                                    </x-table.cell>
                                </x-table.row>
                                @empty
                                <x-table.row>
                                    <x-table.cell colspan="{{count($product['headers'])}}">
                                        <div class="flex justify-center items-center space-x-2">
                                            <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                            <span class="font-medium py-8 text-gray-400 text-xl">No Product found...</span>
                                        </div>
                                    </x-table.cell>
                                </x-table.row>
                                @endforelse
                            </x-slot>
                        </x-table>
                    </div>
                    @endforeach

                </div>
            </div>

            </div>
        </div>
    </form>

    <!-- Delete Product Discount Modal -->
    <form wire:submit.prevent="deleteProduct">
        <x-modal.confirmation wire:model.defer="showConfirmModal">
            <x-slot name="title">Delete Product</x-slot>

            <x-slot name="content">
                <div class="py-8 text-gray-700">Are you sure you? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>


    <!-- Save Product Modal -->
    <form wire:submit.prevent="addProduct">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">{{$currentProduct}}</x-slot>
            <x-slot name="content">
                @switch($currentProduct)
                @case('Roller Blinds')
                <livewire:roller-blind-form />
                @break
                @case('Basswood Shutters')
                <livewire:basswood-shutter-form />
                @break
                @case('PVC Shutters')
                <livewire:pvc-shutter-form />
                @break
                @case('AU PVC Shutters')
                <livewire:au-pvc-shutter-form />
                @break
                @case('Aluminium Shutters')
                <livewire:aluminium-shutter-form />
                @break
                @endswitch
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

</div>
