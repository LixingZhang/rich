<div>
    <h1 class="text-2xl font-semibold text-gray-900">Customers</h1>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="w-1/4">
                <x-input.text wire:model="filters.search" placeholder="Search Company / Product..."/>
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

{{--                @can('delete', \App\Models\ProductDiscount::class)--}}
{{--                    <x-dropdown label="Bulk Actions">--}}
{{--                        --}}{{-- <x-dropdown.item type="button" wire:click="exportSelected" class="flex items-center space-x-2">--}}
{{--                        --}}{{-- <x-icon.download class="text-gray-400"/> <span>Export</span>--}}
{{--                        --}}{{-- </x-dropdown.item>--}}

{{--                        <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')"--}}
{{--                                         class="flex items-center space-x-2">--}}
{{--                            <x-icon.trash class="text-gray-400"/>--}}
{{--                            <span>Delete</span>--}}
{{--                        </x-dropdown.item>--}}
{{--                    </x-dropdown>--}}
{{--                @endcan--}}

                @can('create', \App\Models\Customer::class)
                    <x-button.primary wire:click="newCustomer">
                        <x-icon.plus document class="mr-2"/>
                        New
                    </x-button.primary>
                @endcan
            </div>
        </div>

        <!-- Quotes Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        @if (count($customers))
                            <x-input.checkbox wire:model="selectPage"/>
                        @endif
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('name')"
                                     :direction="$sorts['name'] ?? null">Customer Name
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('tenants.name')"
                                     :direction="$sorts['tenants.name'] ?? null">Company
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('discount')"
                                     :direction="$sorts['discount'] ?? null">Location
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('contact_name')"
                                     :direction="$sorts['contact_name'] ?? null">Contact Name
                    </x-table.heading>
                    <x-table.heading/>
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-gray-200" wire:key="row-message">
                            <x-table.cell colspan="6">
                                @unless ($selectAll)
                                    <div>
                                        <span>You have selected <strong>{{ $customers->count() }}</strong> rows, do you want to select all <strong>{{ $customers->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All
                                        </x-button.link>
                                    </div>
                                @else
                                    <span>You are currently selecting all <strong>{{ $customers->total() }}</strong> rows.</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($customers as $cm)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $cm->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $cm->id }}"/>
                            </x-table.cell>

                            <x-table.cell>
                            <span class="inline-flex space-x-2 truncate text-sm leading-5">
                                <span class="text-gray-600 truncate font-medium">
                                    {{ $cm->name }}
                                </span>
                            </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium">{{ $cm->tenant->name }} </span>
                            </x-table.cell>


                            <x-table.cell>
                                <span class="text-gray-600 font-medium"> {{ $cm->location }} </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium"> {{ $cm->contact_name }} </span>
                            </x-table.cell>

                            <x-table.cell>
                                @can('update', \App\Models\Customer::class)
                                    <x-button.link wire:click="edit({{ $cm->id }})">
                                        <x-icon.edit/>
                                    </x-button.link>
                                @endcan
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="8">
                                <div class="flex justify-center items-center space-x-2">
                                    <x-icon.inbox class="h-8 w-8 text-gray-400"/>
                                    <span class="font-medium py-8 text-gray-400 text-xl">No Customers found...</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $customers->links() }}
            </div>
        </div>
    </div>

    <!-- Delete Product Discount Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">Delete Product Discount</x-slot>

            <x-slot name="content">
                <div class="py-8 text-gray-700">Are you sure you? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Save Product Discount Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">
                @if($customer && !$customer->id) Create @else Edit @endif Customer
            </x-slot>
            <x-slot name="content">
                @if ($customer && !$customer->id)
                    <x-input.group for="tenant" label="Company" :error="$errors->first('customer.tenant_id')">
                        <x-input.select wire:model.defer="customer.tenant_id" id="tenant" placeholder="Please Select..">
                            @foreach(auth()->user()->isTenant() ? auth()->user()->tenants : \App\Models\Tenant::all() as $tenant)
                                <option value="{{ $tenant->id }}">{{ $tenant->name }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group for="name" label="Customer Name" :error="$errors->first('customer.name')">
                        <x-input.text wire:model.defer="customer.name" id="name"></x-input.text>
                    </x-input.group>
                    <x-input.group for="email" label="Email" :error="$errors->first('customer.email')">
                        <x-input.text wire:model.defer="customer.email" id="email"></x-input.text>
                    </x-input.group>
                    <x-input.group for="contact_name" label="Contact Name"
                                   :error="$errors->first('customer.contact_name')">
                        <x-input.text wire:model.defer="customer.contact_name" id="contact_name"></x-input.text>
                    </x-input.group>
                    <x-input.group for="address" label="Address" :error="$errors->first('customer.address')">
                        <x-input.text wire:model.defer="customer.address" id="address"></x-input.text>
                    </x-input.group>

                    <x-input.group for="city" label="City" :error="$errors->first('customer.city')">
                        <x-input.text wire:model.defer="customer.city" id="city"></x-input.text>
                    </x-input.group>

                    <x-input.group for="state" label="State" :error="$errors->first('customer.state')">
                        <x-input.select wire:model.defer="customer.state" id="state" placeholder="Please Select...">
                            @foreach(\App\Models\Address::$states as $state)
                                <option value="{{ $state }}">{{$state}}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group for="postcode" label="Postcode" :error="$errors->first('customer.postcode')">
                        <x-input.text wire:model.defer="customer.postcode" id="postcode"></x-input.text>
                    </x-input.group>
                @endif
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>
</div>
