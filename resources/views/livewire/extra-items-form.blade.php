<div>
    <div class="mt-2 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-5">
        <div class="sm:col-span-2">
            <x-input.group inline for="description" :error="$errors->first('description')">
                <x-input.text wire:model.defer="description" id="description" placeholder="Description"
                ></x-input.text>
            </x-input.group>
        </div>
        <x-input.group inline for="quantity" :error="$errors->first('quantity')">
            <x-input.text wire:model.defer="quantity" id="quantity" placeholder="Quantity"
            ></x-input.text>
        </x-input.group>
        <x-input.group inline for="amount" :error="$errors->first('amount')">
            <x-input.text wire:model.defer="amount" id="amount" placeholder="Price"
            ></x-input.text>
        </x-input.group>
        <x-button.primary class="mt-1" wire:click="addItem">
            <svg class="-ml-2 mr-1 h-5 w-5 text-white" x-description="Heroicon name: plus" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path>
            </svg>
            Add
        </x-button.primary>
    </div>

    <div class="mt-2 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-5">
        @foreach($extraItems as $item)
            <div class="sm:col-span-2">
                <p>{{$item['description']}}</p>
            </div>
            <p class="text-right">{{$item['quantity']}}</p>
            <p class="text-right">$ {{$item['amount']}}</p>
            <x-button class="mt-1 bg-red-700" wire:click="removeItem({{$loop->index}})">
                <svg class="w-4 h-4 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
            </x-button>
        @endforeach
    </div>
</div>
