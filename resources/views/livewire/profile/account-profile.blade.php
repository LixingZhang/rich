<section aria-labelledby="payment_details_heading">
    <form wire:submit.prevent="save">
        <div class="shadow sm:rounded-md sm:overflow-hidden">
            <div class="bg-white py-6 px-4 sm:p-6">
                <div>
                    <h2 id="payment_details_heading" class="text-lg leading-6 font-medium text-gray-900">
                        Bank Account details</h2>
                </div>

                <div class="mt-6 grid grid-cols-4 gap-6">
                    <div class="col-span-3 sm:col-span-2">
                        <x-input.group inline for="account_name" label="Account Name"
                                       :error="$errors->first('bankAccount.account_name')">
                            <x-input.text id="account_name" wire:model.defer="bankAccount.account_name"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-3 sm:col-span-2">
                        <x-input.group inline for="bsb" label="BSB"
                                       :error="$errors->first('bankAccount.bsb')">
                            <x-input.text id="bsb" wire:model.defer="bankAccount.bsb"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-3 sm:col-span-2">
                        <x-input.group inline for="account_number" label="Account Number"
                                       :error="$errors->first('bankAccount.account_number')">
                            <x-input.text id="account_number" wire:model.defer="bankAccount.account_number"></x-input.text>
                        </x-input.group>
                    </div>

                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <x-button.primary type="submit">
                    Save
                </x-button.primary>
            </div>
        </div>
    </form>
</section>
