<section aria-labelledby="payment_details_heading">
    <form wire:submit.prevent="save">
        <div class="shadow sm:rounded-md sm:overflow-hidden">
            <div class="bg-white py-6 px-4 sm:p-6">
                <div>
                    <h2 id="payment_details_heading" class="text-lg leading-6 font-medium text-gray-900">
                        Basic Information</h2>
                </div>

                <div class="mt-6 sm:mt-5">
                    <x-input.group for="name" label="Name" :error="$errors->first('tenant.name')">
                        <x-input.text wire:model.defer="tenant.name" id="name"
                                      placeholder="Company Name"></x-input.text>
                    </x-input.group>
                </div>
                <div class="mt-6 sm:mt-5">
                    <x-input.group for="abn" label="ABN" :error="$errors->first('tenant.abn')">
                        <x-input.text wire:model.defer="tenant.abn" id="abn"
                                      placeholder="ABN"></x-input.text>
                    </x-input.group>
                </div>
                <div class="mt-6 sm:mt-5">
                    <x-input.group label="Logo" for="logo" :error="$errors->first('logo')">
                        <x-input.file-upload wire:model="logo" id="logo">
                    <span class="h-14 w-14 rounded-full overflow-hidden bg-gray-100">
                        @if ($logo && stripos($logo->getMimeType(), 'image') !== false)
                            <img src="{{ $logo->temporaryUrl() }}" alt="Company Logo">
                        @else
                            <img src="{{ $tenant->logoUrl() }}" alt="Company Logo">
                        @endif
                    </span>
                        </x-input.file-upload>
                    </x-input.group>
                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <x-button.primary type="submit">
                    Save
                </x-button.primary>
            </div>
        </div>
    </form>
</section>
