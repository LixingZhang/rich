<section aria-labelledby="payment_details_heading">
    <form wire:submit.prevent="save">
        <div class="shadow sm:rounded-md sm:overflow-hidden">
            <div class="bg-white py-6 px-4 sm:p-6">
                <div>
                    <h2 id="payment_details_heading" class="text-lg leading-6 font-medium text-gray-900">
                        Address details</h2>
                </div>

                <div class="mt-6 grid grid-cols-4 gap-6">
                    <div class="col-span-4 sm:col-span-2">
                        <x-input.group inline for="contact_name" label="Contact Name"
                                       :error="$errors->first('address.contact_name')">
                            <x-input.text id="contact_name" wire:model.defer="address.contact_name"
                                          placeholder="Contact Name"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                        <x-input.group inline for="contact_number" label="Contact Number"
                                       :error="$errors->first('address.contact_number')">
                            <x-input.text id="contact_number" wire:model.defer="address.contact_number"
                                          placeholder="Contact Number"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                        <x-input.group inline for="address_line_1" label="Address Line 1"
                                       :error="$errors->first('address.address_line_1')">
                            <x-input.text id="address_line_1" wire:model.defer="address.address_line_1"
                                          placeholder="Address Line 1"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-1">
                        <x-input.group inline for="address_line_2" label="Address Line 2"
                                       :error="$errors->first('address.address_line_2')">
                            <x-input.text id="address_line_2" wire:model.defer="address.address_line_2"
                                          placeholder="Address Line 2"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-1">
                        <x-input.group inline for="city" label="City"
                                       :error="$errors->first('address.city')">
                            <x-input.text wire:model.defer="address.city" id="city"
                                          placeholder="City"></x-input.text>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                        <x-input.group inline for="state" label="State / Province"
                                       :error="$errors->first('address.state')">
                            <x-input.select wire:model.defer="address.state" id="state"
                                            placeholder="Select...">
                                @foreach(\App\Models\Address::$states as $state)
                                    <option value="{{ $state }}">{{$state}}</option>
                                @endforeach
                            </x-input.select>
                        </x-input.group>
                    </div>

                    <div class="col-span-4 sm:col-span-2">
                        <x-input.group inline for="postcode" label="Post Code"
                                       :error="$errors->first('address.postcode')">
                            <x-input.text wire:model.defer="address.postcode" id="postcode"
                                          placeholder="Post Code"></x-input.text>
                        </x-input.group>
                    </div>
                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <x-button.primary type="submit">
                    Save
                </x-button.primary>
            </div>
        </div>
    </form>
</section>
