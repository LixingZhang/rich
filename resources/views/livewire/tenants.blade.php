<div>
    <div class="px-4 sm:px-6 md:px-0">
        <h1 class="text-3xl font-extrabold text-gray-900">Companies</h1>
    </div>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="w-1/4">
                <x-input.text wire:model="filters.search" placeholder="Search Company Name / ABN" />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                {{--                @can('delete', \App\Models\ProductDiscount::class)--}}
                {{--                    <x-dropdown label="Bulk Actions">--}}
                {{--                        --}}{{-- <x-dropdown.item type="button" wire:click="exportSelected" class="flex items-center space-x-2">--}}
                {{--                        --}}{{-- <x-icon.download class="text-gray-400"/> <span>Export</span>--}}
                {{--                        --}}{{-- </x-dropdown.item>--}}

                {{--                        <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">--}}
                {{--                            <x-icon.trash class="text-gray-400" />--}}
                {{--                            <span>Delete</span>--}}
                {{--                        </x-dropdown.item>--}}
                {{--                    </x-dropdown>--}}
                {{--                @endcan--}}

                @can('create', \App\Models\User::class)
                    <x-button.primary wire:click="create">
                        <x-icon.plus document class="mr-2" />
                        New
                    </x-button.primary>
                @endcan
            </div>
        </div>

        <!-- Quotes Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        @if (count($tenants))
                            <x-input.checkbox wire:model="selectPage" />
                        @endif
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('name')" :direction="$sorts['name'] ?? null">Name
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('abn')" :direction="$sorts['abn'] ?? null">ABN
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('users_count')" :direction="$sorts['users_count'] ?? null">Users Count
                    </x-table.heading>
{{--                    <x-table.heading sortable multi-column wire:click="sortBy('created_at')" :direction="$sorts['created_at'] ?? null">Last Login--}}
{{--                    </x-table.heading>--}}
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-gray-200" wire:key="row-message">
                            <x-table.cell colspan="6">
                                @unless ($selectAll)
                                    <div>
                                        <span>You have selected <strong>{{ $tenants->count() }}</strong> rows, do you want to select all <strong>{{ $tenants->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All
                                        </x-button.link>
                                    </div>
                                @else
                                    <span>You are currently selecting all <strong>{{ $tenants->total() }}</strong> rows.</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($tenants as $tenant)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $tenant->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $tenant->id }}" />
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium">{{ $tenant->name }} </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="inline-flex space-x-2 truncate text-sm leading-5">

                                    <span class="text-gray-600 truncate font-medium">
                                            {{ $tenant->abn }}
                                    </span>
                                </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="text-gray-600 font-medium">
                                           {{ $tenant->users_count }}
                                </span>
                            </x-table.cell>


{{--                            <x-table.cell>--}}
{{--                                <span class="text-gray-600 font-medium"> {{ $tenant->date_for_humans }} </span>--}}
{{--                            </x-table.cell>--}}

                            <x-table.cell>
                                @can('update', $tenant)
                                    <x-button.link wire:click="edit({{ $tenant->id }})">
                                        <x-icon.edit />
                                    </x-button.link>
                                @endcan
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="8">
                                <div class="flex justify-center items-center space-x-2">
                                    <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                    <span class="font-medium py-8 text-gray-400 text-xl">No Users found...</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $tenants->links() }}
            </div>
        </div>
    </div>

{{--    <!-- Delete Product Discount Modal -->--}}
{{--    <form wire:submit.prevent="deleteSelected">--}}
{{--        <x-modal.confirmation wire:model.defer="showDeleteModal">--}}
{{--            <x-slot name="title">Delete User</x-slot>--}}

{{--            <x-slot name="content">--}}
{{--                <div class="py-8 text-gray-700">Are you sure you? This action is irreversible.</div>--}}
{{--            </x-slot>--}}

{{--            <x-slot name="footer">--}}
{{--                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>--}}

{{--                <x-button.primary type="submit">Delete</x-button.primary>--}}
{{--            </x-slot>--}}
{{--        </x-modal.confirmation>--}}
{{--    </form>--}}

<!-- Save Product Discount Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">
                @if($editing && !$editing->id) Create @else Edit @endif Company
            </x-slot>
            <x-slot name="content">
                @if ($editing && !$editing->id)
                    <x-input.group for="name" label="Name" :error="$errors->first('editing.name')">
                        <x-input.text wire:model.defer="editing.name" id="name" placeholder="Name"></x-input.text>
                    </x-input.group>

                    <x-input.group for="abn" label="ABN" :error="$errors->first('editing.abn')">
                        <x-input.text wire:model.defer="editing.abn" id="abn" placeholder="ABN"></x-input.text>
                    </x-input.group>

                    {{--                    <x-input.group for="first_name" label="First Name" :error="$errors->first('editing.first_name')">--}}
                    {{--                        <x-input.text wire:model.defer="editing.first_name" id="first_name" placeholder="First Name"></x-input.text>--}}
                    {{--                    </x-input.group>--}}
                @endif

                {{--                <x-input.group for="discount" label="Discount" :error="$errors->first('editing.discount')">--}}
                {{--                    <x-input.text wire:model.defer="editing.discount" id="discount" placeholder="Discount" trailingAddOn="% OFF"></x-input.text>--}}
                {{--                </x-input.group>--}}
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

</div>
