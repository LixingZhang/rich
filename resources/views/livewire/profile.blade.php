<div x-data="{ tab: @entangle('currentTab') }" x-cloak>
    <!--
  Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
  Read the documentation to get started: https://tailwindui.com/documentation
-->
    <div class="px-4 sm:px-6 md:px-0">
        <h1 class="text-3xl font-extrabold text-gray-900">Settings</h1>
    </div>

    <div class="mt-6 flex w-1/2 space-x-4">
        <div class="w-1/2">
            <x-input.group inline for="tenant" label="Choose Company">
                <x-input.select wire:model="tenantId" id="tenant" placeholder="Please Select...">
                    @foreach(auth()->user()->isTenant() ? auth()->user()->tenants : \App\Models\Tenant::all() as $company )
                        <option value="{{ $company->id }}"> {{ $company->name }} {{$company->isPrimary() ? '(Primary)' : '' }}</option>
                    @endforeach
                </x-input.select>
            </x-input.group>
        </div>
        <x-button.primary wire:click="setPrimary" class="mt-6">
            Set Primary
        </x-button.primary>
    </div>

    <div class="px-4 sm:px-6 md:px-0">
        <div class="py-6">
            <!-- Tabs -->
            <div class="lg:hidden">
                <label for="selected-tab" class="sr-only">Select a tab</label>
                <select id="selected-tab" name="selected-tab"
                        class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                    <option selected>General</option>
                    <option>Address</option>
                    <option>Bank Account</option>
                </select>
            </div>
            <div class="hidden lg:block">
                <div class="border-b border-gray-200">
                    <nav class="-mb-px flex">
                        <!-- Current: "border-indigo-500 text-indigo-600", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" -->
                        <a href="#"
                           wire:click="$set('currentTab', 'general')"
                           :class="{'border-indigo-500 text-indigo-600 hover:text-indigo-600 hover:border-indigo-500': tab == 'general' }"
                           class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                            General
                        </a>
                        <a href="#"
                           wire:click="$set('currentTab', 'address')"
                           :class="{'border-indigo-500 text-indigo-600 hover:text-indigo-600 hover:border-indigo-500': tab == 'address' }"
                           class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 whitespace-nowrap ml-8 py-4 px-1 border-b-2 font-medium text-sm">
                            Address
                        </a>

                        <a href="#"
                           wire:click="$set('currentTab', 'account')"
                           :class="{'border-indigo-500 text-indigo-600 hover:text-indigo-600 hover:border-indigo-500': tab == 'account' }"
                           class="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 whitespace-nowrap ml-8 py-4 px-1 border-b-2 font-medium text-sm">
                            Bank Account
                        </a>

                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="space-y-6">
        <div x-show="tab == 'general'">
            <livewire:profile.tenant-profile key="{{ Str::random() }}" :tenant="$tenant"></livewire:profile.tenant-profile>
        </div>
        <div x-show="tab == 'address'">
            <livewire:profile.address-profile key="{{ Str::random() }}" :tenant="$tenant"></livewire:profile.address-profile>
        </div>
        <div x-show="tab == 'account'">
            <livewire:profile.account-profile key="{{ Str::random() }}" :tenant="$tenant"></livewire:profile.account-profile>
        </div>

    </div>
</div>
