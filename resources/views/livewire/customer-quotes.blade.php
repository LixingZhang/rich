<div>
    <h1 class="text-2xl font-semibold text-gray-900">Customer Quotes</h1>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="w-1/4">
                <x-input.text wire:model="filters.search" placeholder="Search Company / Reference..." />
            </div>

            <div class="space-x-2 flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

{{--                <x-dropdown label="Bulk Actions">--}}
{{--                    --}}{{-- <x-dropdown.item type="button" wire:click="exportSelected" class="flex items-center space-x-2">--}}
{{--                    --}}{{-- <x-icon.download class="text-gray-400"/> <span>Export</span>--}}
{{--                    --}}{{-- </x-dropdown.item>--}}

{{--                    <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">--}}
{{--                        <x-icon.trash class="text-gray-400" />--}}
{{--                        <span>Delete</span>--}}
{{--                    </x-dropdown.item>--}}
{{--                </x-dropdown>--}}

                <x-button.primary wire:click="submitOrder" :disabled="!count($selected)">
                    <x-icon.plus document class="mr-2 " />Submit Order
                </x-button.primary>

                <x-button.primary wire:click="emailForm" :disabled="count($selected) != 1">
                    <x-icon.email class="mr-2" /> Send Email
                </x-button.primary>
            </div>
        </div>
        <!-- Customer Quotes Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        <x-input.checkbox wire:model="selectPage" />
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('tenants.name')" :direction="$sorts['tenants.name'] ?? null">Company Name</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('reference')" :direction="$sorts['reference'] ?? null">Reference</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('markup_type')" :direction="$sorts['markup_type'] ?? null">Markup Type</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('markup_value')" :direction="$sorts['markup_value'] ?? null">Markup Value</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null">Status</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('created_at')" :direction="$sorts['created_at'] ?? null">Date</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                    <x-table.row class="bg-gray-200" wire:key="row-message">
                        <x-table.cell colspan="6">
                            @unless ($selectAll)
                            <div>
                                <span>You have selected <strong>{{ $quotes->count() }}</strong> transactions, do you want to select all <strong>{{ $quotes->total() }}</strong>?</span>
                                <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All</x-button.link>
                            </div>
                            @else
                            <span>You are currently selecting all <strong>{{ $quotes->total() }}</strong> transactions.</span>
                            @endif
                        </x-table.cell>
                    </x-table.row>
                    @endif

                    @forelse ($quotes as $qt)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $qt->customerQuote->id }}">
                        <x-table.cell class="pr-0">
                            <x-input.checkbox wire:model="selected" value="{{ $qt->customerQuote->id }}" />
                        </x-table.cell>

                        <x-table.cell>
                            <span class="inline-flex space-x-2 truncate text-sm leading-5">
                                {{-- <x-icon.cash class="text-gray-400"/>--}}

                                <span class="text-gray-600 truncate font-medium">
                                    {{ $qt->tenant->name }}
                                </span>
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $qt->reference }} </span>
                        </x-table.cell>


                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $qt->customerQuote->markup_type }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $qt->customerQuote->markup_value }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $qt->customerQuote->status_colour }}-100 text-{{ $qt->customerQuote->status_colour }}-800 capitalize">
                                {{ $qt->customerQuote->status }}
                            </span>
                        </x-table.cell>


                        <x-table.cell>
                            <span class="text-gray-600 font-medium"> {{ $qt->date_for_humans }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <x-button.link wire:click="edit({{ $qt->customerQuote->id }})">
                                <x-icon.edit />
                            </x-button.link>

                            <x-button.link wire:click="extraItems({{ $qt->customerQuote->id }})">
                                <svg class="text-red-700 inline-block w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                            </x-button.link>
                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="8">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                <span class="font-medium py-8 text-gray-400 text-xl">No Customer Quotes found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $quotes->links() }}
            </div>
        </div>
    </div>

    <!-- Delete Product Discount Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">Delete Customer Quotes</x-slot>

            <x-slot name="content">
                <div class="py-8 text-gray-700">Are you sure you? This action is irreversible.</div>
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">Cancel</x-button.secondary>

                <x-button.primary type="submit">Delete</x-button.primary>
            </x-slot>
        </x-modal.confirmation>
    </form>

    <!-- Save Product Discount Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">Edit Customer Quotes</x-slot>
            <x-slot name="content">
                <x-input.group for="markup_type" label="Markup Type" :error="$errors->first('editing.markup_type')">
                    <x-input.select wire:model.lazy="editing.markup_type" id="discount" placeholder="Please Select...">
                        <option value="fixed">Fixed Markup</option>
                        <option value="percent">Percentage Markup</option>
                    </x-input.select>
                </x-input.group>
                <x-input.group for="markup_value" label="Markup Value" :error="$errors->first('editing.markup_value')">
                    <x-input.text wire:model.defer="editing.markup_value" id="markup_value" placeholder="Markup Value"
                              trailingAddOn="{{ $editing && $editing->markup_type == \App\Models\CustomerQuote::MARKUP_PERCENT ? '%' : 'AUD' }}"
                    >
                    </x-input.text>
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>

    @if($showExtraItemsModel)
        <form wire:submit.prevent="saveExtraItems">
            <x-modal.dialog wire:model.defer="showExtraItemsModel">
                <x-slot name="title">Extra Items</x-slot>
                <x-slot name="content">
                    @livewire('extra-items-form', ['customerQuote' => $editing])
                </x-slot>
                <x-slot name="footer">
                    <x-button.secondary wire:click="$set('showExtraItemsModel', false)">Cancel</x-button.secondary>
                    <x-button.primary type="submit">Save</x-button.primary>
                </x-slot>
            </x-modal.dialog>
        </form>
    @endif

    @if($showEmailModal)
        <form wire:submit.prevent="sendEmail">
            <x-modal.dialog wire:model.defer="showEmailModal">
                <x-slot name="title">Send Email</x-slot>
                <x-slot name="content">
                    @livewire('email-form', ['subject' => $subject, 'content' => $content])
                </x-slot>
                <x-slot name="footer">
                    <x-button.secondary wire:click="$set('showEmailModal', false)">Cancel</x-button.secondary>
                    <x-button.primary type="submit">Send</x-button.primary>
                </x-slot>
            </x-modal.dialog>
        </form>
    @endif
</div>
