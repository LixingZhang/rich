<div>
    <h1 class="text-2xl font-semibold text-gray-900">Orders</h1>

    <div class="py-4 space-y-4">
        <div class="flex justify-between">
            <div class="flex space-x-4 w-2/4">
                <div class="w-1/2">
                    <x-input.text wire:model="filters.search" placeholder="Search Company / Reference..." />
                </div>
                <x-button.link wire:click="$toggle('showFilters')">@if ($showFilters) Hide @endif Advanced Search
                </x-button.link>
            </div>
            <div class="space-x-2 lg:flex items-center">
                <x-input.group borderless paddingless for="perPage" label="Per Page">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>
            </div>
        </div>

        <div>
            @if ($showFilters)
            <div class="bg-gray-200 p-4 rounded shadow-inner lg:flex relative">
                <div class="w-1/2 pr-2 space-y-4">
                    <x-input.group inline for="filter-tenant" label="Company">
                        <x-input.select wire:model="filters.tenant" id="filter-tenant">
                            <option value="" disabled>Select Company...</option>
                            @foreach (auth()->user()->isTenant() ? auth()->user()->tenants : \App\Models\Tenant::all() as $tenant)
                                <option value="{{ $tenant->id }}">{{ $tenant->name }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group inline for="filter-status" label="Status">
                        <x-input.select wire:model="filters.status" id="filter-status">
                            <option value="" disabled>Select Status...</option>

                            @foreach (App\Models\Order::STATUSES as $value => $label)
                            <option value="{{ $value }}">{{ $label }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>
                </div>

                <div class="w-1/2 pr-2 space-y-4">
                    <x-input.group inline for="filter-amount-min" label="Minimum Amount">
                        <x-input.money wire:model.lazy="filters.amount-min" id="filter-amount-min" />
                    </x-input.group>


                    <x-input.group inline for="filter-amount-max" label="Maximum Amount">
                        <x-input.money wire:model.lazy="filters.amount-max" id="filter-amount-max" />
                    </x-input.group>
                </div>

                <div class="w-1/2 pr-2 space-y-4">
                    <x-input.group inline for="filter-date-min" label="Minimum Date">
                        <x-input.date wire:model="filters.date-min" id="filter-date-min" placeholder="MM/DD/YYYY" />
                    </x-input.group>

                    <x-input.group inline for="filter-date-max" label="Maximum Date">
                        <x-input.date wire:model="filters.date-max" id="filter-date-max" placeholder="MM/DD/YYYY" />
                    </x-input.group>
                </div>

                <div class="w-1/2 pr-2 space-y-4">
                    <x-button.link wire:click="resetFilters" class="absolute right-0 bottom-0 p-4">Reset Filters
                    </x-button.link>
                </div>
            </div>
            @endif

        </div>

        <!-- Orders Table -->
        <div class="flex-col space-y-4">
            <x-table>
                <x-slot name="head">
                    <x-table.heading class="pr-0 w-8">
                        @if (count($orders))
                            <x-input.checkbox wire:model="selectPage" />
                        @endif
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('tenants.name')" :direction="$sorts['tenants.name'] ?? null">Company
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('users.name')" :direction="$sorts['users.name'] ?? null">Staff
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('reference')" :direction="$sorts['reference'] ?? null">Reference
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('product')" :direction="$sorts['product'] ?? null">Product
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('amount')" :direction="$sorts['amount'] ?? null">Amount
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('status')" :direction="$sorts['status'] ?? null">Status
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('eta')" :direction="$sorts['eta'] ?? null">ETA
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('created_at')" :direction="$sorts['created_at'] ?? null">Date
                    </x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                    <x-table.row class="bg-gray-200" wire:key="row-message">
                        <x-table.cell colspan="8">
                            @unless ($selectAll)
                            <div>
                                <span>You have selected <strong>{{ $orders->count() }}</strong> orders, do you want to select all <strong>{{ $orders->total() }}</strong>?</span>
                                <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">Select All
                                </x-button.link>
                            </div>
                            @else
                            <span>You are currently selecting all <strong>{{ $orders->total() }}</strong> orders.</span>
                            @endif
                        </x-table.cell>
                    </x-table.row>
                    @endif

                    @forelse ($orders as $order)
                    <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $order->id }}">
                        <x-table.cell class="pr-0">
                            <x-input.checkbox wire:model="selected" value="{{ $order->id }}" />
                        </x-table.cell>

                        <x-table.cell>
                            <span class="inline-flex space-x-2 truncate text-sm leading-5">

                                <span class="text-gray-600 truncate font-medium">
                                    {{ $order->tenant->name }}
                                </span>
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $order->staff->name }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $order->reference }} </span>
                        </x-table.cell>


                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $order->product }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">$ {{ $order->amount }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-{{ $order->status_colour }}-100 text-{{ $order->status_colour }}-800 capitalize">
                                {{ $order->status }}
                            </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium">{{ $order->eta ? $order->eta->format('d/m/Y') : null }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            <span class="text-gray-600 font-medium"> {{ $order->date_for_humans }} </span>
                        </x-table.cell>

                        <x-table.cell>
                            @can('update', \App\Models\Order::class)
                            <x-button.link wire:click="edit({{ $order->id }})">
                                <x-icon.edit />
                            </x-button.link>
                            @endcan
                        </x-table.cell>
                    </x-table.row>
                    @empty
                    <x-table.row>
                        <x-table.cell colspan="8">
                            <div class="flex justify-center items-center space-x-2">
                                <x-icon.inbox class="h-8 w-8 text-gray-400" />
                                <span class="font-medium py-8 text-gray-400 text-xl">No Orders found...</span>
                            </div>
                        </x-table.cell>
                    </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $orders->links() }}
            </div>
        </div>
    </div>

    <!-- Save Product Discount Modal -->
    <form wire:submit.prevent="save">
        <x-modal.dialog wire:model.defer="showEditModal">
            <x-slot name="title">Edit Order</x-slot>
            <x-slot name="content">
                <x-input.group for="status" label="Status" :error="$errors->first('editing.status')">
                    <x-input.select wire:model.defer="editing.status" id="status" placeholder="Please Select...">
                        @foreach($editing->getStatuses() as $label)
                            <option value="{{$label}}">{{ucfirst($label)}}</option>
                        @endforeach
                    </x-input.select>
                </x-input.group>
                <x-input.group for="eta" label="ETA" :error="$errors->first('editing.eta_for_editing')">
                    <x-input.date wire:model.defer="editing.eta_for_editing" id="eta" placeholder="DD/MM/YYYY"></x-input.date>
                </x-input.group>
            </x-slot>
            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showEditModal', false)">Cancel</x-button.secondary>
                <x-button.primary type="submit">Save</x-button.primary>
            </x-slot>
        </x-modal.dialog>
    </form>
</div>
