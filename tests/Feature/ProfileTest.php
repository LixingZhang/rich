<?php

namespace Tests\Feature;

use App\Http\Livewire\Profile;
use App\Models\Address;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Livewire\Livewire;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_view_profile_page()
    {
        Tenant::factory()->count(3)->create();
        $user = User::factory()->create();

        $this->actingAs($user);

        $this->get(route('profile'))
            ->assertSuccessful()
            ->assertSeeLivewire('profile');
    }

    /** @test */
    public function can_create_profile()
    {
        Event::fake();

        Tenant::factory()->count(3)->create();
        $user = User::factory()->create();

        $this->actingAs($user);

        $address = Address::factory()->make();

        Livewire::test(Profile::class)
            ->set('shippingAddress.contact_name', $address->contact_name)
            ->set('shippingAddress.contact_number', $address->contact_number)
            ->set('shippingAddress.address_line_1', $address->address_line_1)
            ->set('shippingAddress.address_line_2', $address->address_line_2)
            ->set('shippingAddress.city', $address->city)
            ->set('shippingAddress.state', $address->state)
            ->set('shippingAddress.postcode', $address->postcode)
            ->call('save');

        $this->assertTrue(Address::where('contact_name', $address->contact_name)->exists());
    }
}
