<?php

namespace Tests\Unit;

use App\Models\BasswoodShutter;
use App\Service\ShutterPriceCalculator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ShutterPriceCalculatorTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_shutter_pricing()
    {
        $this->seed();

        $basswoodShutter = BasswoodShutter::factory()->create([
            'width' => 1000,
            'drop'  => 1200,
            'shutter_type' => 'Standard',
            'corner' => '90',
            'colour' => 'W100 Snow'
        ]);

        $calculator = new ShutterPriceCalculator($basswoodShutter);

        $this->assertEquals(187, $calculator->getUnitPrice());
        $this->assertEquals(224.4, $calculator->getCost());
    }
}
