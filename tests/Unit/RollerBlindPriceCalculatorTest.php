<?php

namespace Tests\Unit;

use App\Models\RollerBlind;
use App\Service\RollerBlindPriceCalculator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RollerBlindPriceCalculatorTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_roller_blind_pricing()
    {
        $this->seed();

        $rollerBlind = RollerBlind::factory()->make([
            'width' => 1000,
            'drop'  => 1200,
            'fabric' => 'Gala',
            'type' => 'Blockout',
            'colour' => 'Anchor'
        ]);

        $calculator = new RollerBlindPriceCalculator($rollerBlind);

        $this->assertEquals(6, $calculator->getPricingGroup());
        $this->assertEquals(147, $calculator->getUnitPrice());
        $this->assertEquals(176.4, $calculator->getCost());
    }

}
