<?php

use App\Http\Controllers\ApprovalQuoteController;
use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\CustomerQuoteController;
use App\Http\Controllers\CustomerQuotePDFController;
use App\Http\Controllers\OrderPDFController;
use App\Http\Controllers\QuotePDFController;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Passwords\Confirm;
use App\Http\Livewire\Auth\Passwords\Email;
use App\Http\Livewire\Auth\Passwords\Reset;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Auth\Verify;
use App\Http\Livewire\CustomerQuotes;
use App\Http\Livewire\Customers;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\Orders;
use App\Http\Livewire\ProductDiscounts;
use App\Http\Livewire\Profile;
use App\Http\Livewire\Quotes;
use App\Http\Livewire\Tenants;
use App\Http\Livewire\Users;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::redirect('/', 'dashboard');

Route::middleware('guest')->group(function () {
    Route::get('login', Login::class)
        ->name('login');

//    Route::get('register', Register::class)
//        ->name('register');
});

Route::get('password/reset', Email::class)
    ->name('password.request');

Route::get('password/reset/{token}', Reset::class)
    ->name('password.reset');

Route::middleware('auth')->group(function () {
    Route::get('email/verify', Verify::class)
        ->middleware('throttle:6,1')
        ->name('verification.notice');

    Route::get('password/confirm', Confirm::class)
        ->name('password.confirm');
});

Route::get('/customer-quotes/{customerQuote}', [CustomerQuoteController::class, 'show'])
    ->whereNumber('customerQuote')->name('customer-quotes.show');

Route::get('/customer-quotes/{customerQuote}/approve', [ApprovalQuoteController::class, 'store'])
    ->whereNumber('customerQuote')->name('customer-quotes.approve');

Route::get('customer-quotes/{customerQuote}.pdf', CustomerQuotePDFController::class)
    ->name('customer-quotes.pdf');

Route::middleware('auth')->group(function () {
    Route::get('/', Dashboard::class)->name('home');
    Route::get('/orders', Orders::class)->name('orders');
    Route::get('/quotes', Quotes::class)->name('quotes');
    Route::get('/users', Users::class)->name('users');
    Route::get('/tenants', Tenants::class)->name('tenants');
    Route::get('/customers', Customers::class)->name('customers');
    Route::get('/customer-quotes', CustomerQuotes::class)->name('customer-quotes');
    Route::get('/product-discounts', ProductDiscounts::class)->name('product-discounts');
    Route::get('/profile', Profile::class)->name('profile');

    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
        ->middleware('signed')
        ->name('verification.verify');

    Route::get('logout', LogoutController::class)
        ->name('logout');

    Route::get('quotes/{quote}', QuotePDFController::class)
        ->name('quote.pdf');

    Route::get('orders/{order}', OrderPDFController::class)
        ->name('order.pdf');
});


