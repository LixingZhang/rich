<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShutterPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shutter_pricings', function (Blueprint $table) {
            $table->id();
            $table->string('shutter');
            $table->string('attribute');
            $table->string('value');
            $table->decimal('price', 10, 2);
            $table->enum('unit', ['sqm', 'order']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shutter_pricings');
    }
}
