<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomerToCustomerQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_quotes', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\Customer::class)->after('quote_id')
                ->constrained('customers', 'id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_quotes', function (Blueprint $table) {
            //
        });
    }
}
