<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->morphs('productable');
            $table->foreignIdFor(\App\Models\Order::class)->index()
                ->constrained('orders', 'id');
            $table->foreignIdFor(\App\Models\ProductDiscount::class, 'discount_id')->nullable()
                ->constrained('product_discounts', 'id');
            $table->decimal('unit_price', 10, 2);
            $table->decimal('discount_value', 10, 2)->nullable();
            $table->decimal('markup_value', 10, 2)->nullable();
            $table->decimal('amount', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
