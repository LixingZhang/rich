<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRollerBlindPricingGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roller_blind_pricing_groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('group');
            $table->unsignedSmallInteger('width_lower_threshold');
            $table->unsignedSmallInteger('width_upper_threshold');
            $table->unsignedSmallInteger('drop_lower_threshold');
            $table->unsignedSmallInteger('drop_upper_threshold');
            $table->unsignedSmallInteger('price');
            $table->string('unit')->default('sqm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roller_blind_pricing_groups');
    }
}
