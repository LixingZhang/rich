<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRollerBlindPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roller_blind_pricings', function (Blueprint $table) {
            $table->id();
            $table->string('fabric');
            $table->string('type');
            $table->string('colour');
            $table->unsignedTinyInteger('price_group');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roller_blind_pricings');
    }
}
