<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Tenant::class)->index()
                ->constrained('tenants', 'id');
            $table->foreignIdFor(\App\Models\User::class, 'staff_id')->index()
                ->constrained('users', 'id');
            $table->string('reference')->index();
            $table->string('product')->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->string('status');
            $table->timestamp('eta')->nullable();
            $table->string('notes')->nullable();
            $table->foreignIdFor(\App\Models\Address::class, 'shipping_address_id')->nullable()
                ->constrained('addresses', 'id');
            $table->foreignIdFor(\App\Models\Address::class, 'billing_address_id')->nullable()
                ->constrained('addresses', 'id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
