<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\CustomerQuote;
use App\Models\Quote;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerQuoteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomerQuote::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'quote_id' => Quote::factory(),
            'markup_type' => 'percent',
            'markup_value' => $this->faker->numberBetween(0, 100),
            'customer_id' => Customer::factory(),
        ];
    }
}
