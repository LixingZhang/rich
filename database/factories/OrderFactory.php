<?php

namespace Database\Factories;

use App\Models\BasswoodShutter;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RollerBlind;
use App\Models\Tenant;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference' => $this->faker->lexify('????????'),
            'product' => RollerBlind::NAME,
            'tenant_id' => 1,
            'staff_id' => 3,
            'status' => 'completed',
            'notes' => $this->faker->text(150),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Order $order) {
            //
        })->afterCreating(function (Order $order) {
            $product = RollerBlind::factory()->create();
            $item = OrderItem::createFromProduct($product);
            $item->amount = $item->unit_price - $item->discount_value + $item->markup_value;
            $order->amount = $item->amount;
            $order->items()->saveMany([$item]);
            $order->save();
        });
    }
}
