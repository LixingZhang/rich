<?php

namespace Database\Factories;

use App\Models\QuoteItem;
use App\Models\RollerBlind;
use App\Models\Tenant;
use App\Models\Quote;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuoteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Quote::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference' => $this->faker->lexify('??????????'),
            'tenant_id' => 1,
            'staff_id' => 3,
            'notes' => $this->faker->text(150),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Quote $quote) {
            //
        })->afterCreating(function (Quote $quote) {
            $product = RollerBlind::factory()->create();
            $item = QuoteItem::createFromProduct($product);
            $item->amount = $item->unit_price - $item->discount_value + $item->markup_value;
            $quote->amount = $item->amount;
            $quote->items()->saveMany([$item]);
            $quote->save();
        });
    }
}
