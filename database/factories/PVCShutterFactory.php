<?php

namespace Database\Factories;

use App\Models\PvcShutter;
use Illuminate\Database\Eloquent\Factories\Factory;

class PVCShutterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PvcShutter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(10),
            'width' => $this->faker->numberBetween(1000),
            'drop' => $this->faker->numberBetween(1200),
            'shutter_type' => 'Bifold Track',
            'corner' => 'No',
            'panel_layout' => 'LR',
            'panel_qty' => 2,
            'in_or_out' => 'In',
            'colour' => 'white',
            'mid_rail' => 'n/a',
            'mid_rail_height' => 'n/a',
            'blade_size' => '64mm',
            'tilt_rod' => 'Clear View',
            'stile_type' => '50b',
            'frame' => 'FL50-B9',
            'frame_options' => 'LRTB',
            'install_method' => 'Hinged',
            'hinge_type' => 'NM Higne',
            'hinge_colour' => 'Hinge Colour',
        ];
    }
}
