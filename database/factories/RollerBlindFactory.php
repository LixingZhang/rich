<?php

namespace Database\Factories;

use App\Models\RollerBlind;
use Illuminate\Database\Eloquent\Factories\Factory;

class RollerBlindFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RollerBlind::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(10),
            'width' => $this->faker->numberBetween(600, 1000),
            'drop' => $this->faker->numberBetween(600, 1200),
            'fabric' => 'Gala',
            'type'  => 'Blockout',
//            'bracket_colour' => 'white',
            'bracket_options' => '',
            'colour' => 'Anchor',
            'base_rail' => 'silver',
            'component_colour' => '',
            'roll_direction' => 'standard',
            'control_type' => 'motor',
            'control_side' => 'right',
            'chain_length' => 'n/a',
//            'motor_brand' => 'Acemda',
            'motor_type' => 'hard_wired',
            'charger' => 'no',
            'wifi_hub' => 'Yes',
            'remote' => '',
            'notes' => $this->faker->text(150),
        ];
    }
}
