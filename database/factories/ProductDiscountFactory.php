<?php

namespace Database\Factories;

use App\Models\ProductDiscount;
use App\Models\RollerBlind;
use App\Models\Tenant;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductDiscountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductDiscount::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tenant_id' => Tenant::factory(),
            'product' => (new RollerBlind())->getName(),
            'discount' => $this->faker->numberBetween(0, 100)
        ];
    }
}
