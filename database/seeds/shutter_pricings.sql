-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: rich
-- Generation Time: 2020-10-27 11:34:54.2660
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('1', 'Basswood Shutter', 'Shutter Type', 'Standard', '165.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('2', 'Basswood Shutter', 'Shutter Type', 'Sliding', '230.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('3', 'Basswood Shutter', 'Shutter Type', 'Bifold Track', '220.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('4', 'Basswood Shutter', 'Shutter Type', 'U Channel', '165.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('5', 'Basswood Shutter', 'Corner', 'No', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('6', 'Basswood Shutter', 'Corner', 'No', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('7', 'Basswood Shutter', 'Corner', '90', '22.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('8', 'Basswood Shutter', 'Corner', '135', '22.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('9', 'Basswood Shutter', 'Colour', 'W100 Snow', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('10', 'Basswood Shutter', 'Colour', 'W101 Dove', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('11', 'Basswood Shutter', 'Colour', 'W104 China', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('12', 'Basswood Shutter', 'Colour', 'W105 Soft Pearl', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('13', 'Basswood Shutter', 'Colour', 'W107 Pearl', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('14', 'Basswood Shutter', 'Colour', 'W109 Linen Wash', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('15', 'Basswood Shutter', 'Colour', 'W115 Clay', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('16', 'Basswood Shutter', 'Colour', 'W118 Wheat', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('17', 'Basswood Shutter', 'Colour', 'W118 Wheat', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('18', 'Basswood Shutter', 'Colour', 'W401 Eggshell', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('19', 'Basswood Shutter', 'Colour', 'W402 Palm Beach', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('20', 'Basswood Shutter', 'Colour', 'W00 Custom', '150.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('21', 'Basswood Shutter', 'Colour', 'S430 Black Ash', '20.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('22', 'Basswood Shutter', 'Colour', 'S508 Jet', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('23', 'Basswood Shutter', 'Colour', 'S602 Maple', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('24', 'Basswood Shutter', 'Colour', 'S604 Aged Teak', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('25', 'Basswood Shutter', 'Colour', 'S606 Mahogan', '20.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('26', 'Basswood Shutter', 'Colour', 'S701 Walnut', '20.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('27', 'Basswood Shutter', 'Frame', 'No Frame', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('28', 'Basswood Shutter', 'Frame', 'FL50-B9', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('29', 'Basswood Shutter', 'Frame', 'BL50-B10', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('30', 'Basswood Shutter', 'Frame', 'BC65-B14A', '9.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('31', 'Basswood Shutter', 'Frame', 'BC50-B14', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('32', 'Basswood Shutter', 'Frame', 'Z35-C7', '22.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('33', 'Basswood Shutter', 'Frame', 'Z20-C6', '11.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('34', 'Basswood Shutter', 'Frame', 'CZ25-D1', '15.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('35', 'Basswood Shutter', 'Frame', 'CZ55-D2', '35.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('36', 'Basswood Shutter', 'Frame', 'BL65-B10A', '9.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('37', 'PVC Shutter', 'Shutter Type', 'Standard', '162.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('38', 'PVC Shutter', 'Shutter Type', 'Sliding', '230.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('39', 'PVC Shutter', 'Shutter Type', 'Bifold Track', '220.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('40', 'PVC Shutter', 'Shutter Type', 'U Channel', '162.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('41', 'PVC Shutter', 'Corner', 'No', '0.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('42', 'PVC Shutter', 'Corner', '90', '22.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('43', 'PVC Shutter', 'Corner', '135', '22.00', 'sqm', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('44', 'PVC Shutter', 'Colour', 'W100 Snow', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('45', 'PVC Shutter', 'Colour', 'W101 Dove', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('46', 'PVC Shutter', 'Colour', 'W104 China', '0.00', 'order', '2020-10-27 11:34:10', '2020-10-27 11:34:10');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('47', 'PVC Shutter', 'Colour', 'W105 Soft Pearl', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('48', 'PVC Shutter', 'Colour', 'W107 Pearl', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('49', 'PVC Shutter', 'Colour', 'W109 Linen Wash', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('50', 'PVC Shutter', 'Colour', 'W115 Clay', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('51', 'PVC Shutter', 'Colour', 'W118 Wheat', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('52', 'PVC Shutter', 'Colour', 'W401 Eggshell', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('53', 'PVC Shutter', 'Colour', 'W402 Palm Beach', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('54', 'PVC Shutter', 'Colour', 'W00 Custom', '150.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('55', 'PVC Shutter', 'Frame', 'No Frame', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('56', 'PVC Shutter', 'Frame', 'Z20-C6', '11.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('57', 'PVC Shutter', 'Frame', 'BL50-B10', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('58', 'PVC Shutter', 'Frame', 'BL65-B10A', '9.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('59', 'AU PVC Shutter', 'Shutter Type', 'Standard', '190.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('60', 'AU PVC Shutter', 'Shutter Type', 'Sliding', '250.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('61', 'AU PVC Shutter', 'Shutter Type', 'Bifold Track', '240.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('62', 'AU PVC Shutter', 'Shutter Type', 'U Channel', '190.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('63', 'AU PVC Shutter', 'Corner', 'No', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('64', 'AU PVC Shutter', 'Corner', '90', '22.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('65', 'AU PVC Shutter', 'Corner', '135', '22.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('66', 'AU PVC Shutter', 'Colour', 'White', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('67', 'AU PVC Shutter', 'Colour', 'Off White', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('68', 'AU PVC Shutter', 'Colour', 'Custom', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('69', 'AU PVC Shutter', 'Frame', 'No Frame', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('70', 'AU PVC Shutter', 'Frame', 'Z20-C6', '11.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('71', 'AU PVC Shutter', 'Frame', 'BL50-B10', '11.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('72', 'AU PVC Shutter', 'Frame', 'BL65-B10A', '11.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('73', 'Aluminium Shutter', 'Shutter Type', 'Standard', '229.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('74', 'Aluminium Shutter', 'Shutter Type', 'Sliding', '250.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('75', 'Aluminium Shutter', 'Shutter Type', 'Bifold Track', '250.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('76', 'Aluminium Shutter', 'Shutter Type', 'U Channel', '229.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('77', 'Aluminium Shutter', 'Corner', 'No', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('78', 'Aluminium Shutter', 'Corner', '90', '22.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('79', 'Aluminium Shutter', 'Corner', '135', '22.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('80', 'Aluminium Shutter', 'Colour', 'White', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('81', 'Aluminium Shutter', 'Colour', 'Cream', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('82', 'Aluminium Shutter', 'Colour', 'Silver', '0.00', 'order', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('83', 'Aluminium Shutter', 'Frame', 'No Frame', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('84', 'Aluminium Shutter', 'Frame', 'L', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('85', 'Aluminium Shutter', 'Frame', 'U Channel', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('86', 'Aluminium Shutter', 'Frame', 'Bi Fold', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');
INSERT INTO `shutter_pricings` (`id`, `shutter`, `attribute`, `value`, `price`, `unit`, `created_at`, `updated_at`) VALUES ('87', 'Aluminium Shutter', 'Frame', 'Sliding', '0.00', 'sqm', '2020-10-27 11:34:11', '2020-10-27 11:34:11');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
