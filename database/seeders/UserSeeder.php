<?php

namespace Database\Seeders;

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = new Tenant();
        $tenant->name = 'Company One';
        $tenant->abn = '1111111111';
        $tenant->save();

        $tenant2 = new Tenant();
        $tenant2->name = 'Company Two';
        $tenant2->abn = '2222222222';
        $tenant2->save();

        $user = new User();
        $user->name = 'Rich Admin';
        $user->email = 'admin@rich.com';
        $user->password = bcrypt('password');
        $user->role = User::ROLE_ADMIN;
        $user->save();

        $user = new User();
        $user->name = 'Rich User';
        $user->email = 'user@rich.com';
        $user->password = bcrypt('password');
        $user->role = User::ROLE_USER;
        $user->save();

        $user = new User();
        $user->name = 'Company Admin';
        $user->email = 'admin@company.com';
        $user->password = bcrypt('password');
        $user->role = User::ROLE_ADMIN;
        $user->save();
        $user->tenants()->saveMany([$tenant, $tenant2]);

        $user = new User();
        $user->name = 'Company User';
        $user->email = 'user@company.com';
        $user->password = bcrypt('password');
        $user->role = User::ROLE_USER;
        $user->save();
        $user->tenants()->saveMany([$tenant, $tenant2]);
    }
}
