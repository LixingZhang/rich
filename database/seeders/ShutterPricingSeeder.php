<?php

namespace Database\Seeders;

use App\Models\AluminiumShutter;
use App\Models\AuPvcShutter;
use App\Models\BasswoodShutter;
use App\Models\PvcShutter;
use App\Models\ShutterPricing;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShutterPricingSeeder extends Seeder
{
    public function run()
    {
        $path = base_path() . '/database/seeds/shutter_pricings.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    }
}
