<?php
namespace Database\Seeders;

use App\Models\OrderItem;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(RollerBlindPricingGroupSeeder::class);
        $this->call(RollerBlindPricingSeeder::class);
        $this->call(ShutterPricingSeeder::class);


//        $this->call(OrderSeeder::class);
//        $this->call(QuoteSeeder::class);
//        $this->call(CustomerQuoteSeeder::class);
//        $this->call(ProductDiscountSeeder::class);
    }
}
