<?php

namespace Database\Seeders;

use App\Models\CustomerQuote;
use Illuminate\Database\Seeder;

class CustomerQuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerQuote::factory()->count(20)->create();
    }
}
